from enum import Enum
import socket
import signal, sys
import random
import time

DistanceMeasureIntervalInMillis = 500.0
FrameSize = 6

# Commands
StopSending = 8
StartSending = 9

# Responses
Ok = 16
Nok = 17

# IDs
Pc = 4

class ClientSocket:
    def __init__(self):
        self.socket = None
        self.active = False

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def checkIfResponse(data):
    if (data):
        return data[0] == 0xff and data[1] == 0xff and data[2] == 0xff and data[3] == 0xff and data[4] == 0xff
    return False

def onClose (signal, frame):
    server.close()
    sys.exit(0)

def readBytesFromSocket(s, dataSize):
    data = None
    try:
        data = s.recv(dataSize)
    except BlockingIOError:
        pass

    return data

def sendResponse(s, response):
    messageToSend = bytes([0xff, 0xff, 0xff, 0xff, 0xff, int(response)])
    s.send(messageToSend)

def sendSensorData(client):
    leftSensorReading = random.randint(2, 200)
    rightSensorReading = random.randint(2, 200)

    data = bytes([0x03, 0x01, rightSensorReading, rightSensorReading >> 8, leftSensorReading, leftSensorReading >> 8])
    client.socket.send(data)
    time.sleep(0.5)

def main():
    signal.signal(signal.SIGINT, onClose)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    host = '127.0.0.1'
    port = 5000
    
    server.bind((host, port))
    server.listen(1)

    pcClient = ClientSocket()
    
    while True:
        incomingClient, _ = server.accept()
        if incomingClient:
            print('New client connected...')
            response = readBytesFromSocket(incomingClient, FrameSize)

            if checkIfResponse(response):
                print("Correct client's response")
                clientId = int(response[FrameSize - 1])

                if clientId == Pc:
                    print("Pc device connected.")
                    pcClient.socket = incomingClient
                    pcClient.socket.setblocking(False)
                    sendResponse(incomingClient, Ok)
                else:
                    print("Unknown device connected.")
                    sendResponse(incomingClient, Nok)
        else:
            print("Wrong client's response.")

        while pcClient.socket:
            try:
                incomingMessage = readBytesFromSocket(pcClient.socket, FrameSize)

                if (incomingMessage and checkIfResponse(incomingMessage)):
                    command = int(incomingMessage[FrameSize - 1])

                    if (command == StartSending):
                        print("Command: StartSending")
                        sendResponse(pcClient.socket, Ok)
                        pcClient.active = True

                    elif (command == StopSending):
                        print("Command: StopSending")
                        sendResponse(pcClient.socket, Ok)
                        pcClient.active = False
                
                if pcClient.active:
                    print("Sending distance...")
                    sendSensorData(pcClient)

            except socket.error as e:
                if e.errno == errno.ECONNRESET:
                    pcClient.socket.close()
                else:
                    raise e
                pcClient.socket = None



if __name__ == "__main__":
    main()
