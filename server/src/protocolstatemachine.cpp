#include "protocolstatemachine.h"
#include "entrymodel.h"
#include "simulationmodel.h"
#include "modelcontentcreator.h"

#include "sl_argument.h"
#include "sl_debug.h"
#include "sl_qmlutils.h"
using namespace SimulationLib;

ProtocolStateMachine::ProtocolStateMachine(
        EntryModel* entryModel,
        ConnectionClient* sensorClient,
        QObject *parent)
    :
    QObject                     (parent),
    m_sensorClient              (sensorClient),
    m_entryModel                (entryModel),
    m_idle                      (new QState()),
    m_startReceivingFromSensor  (new QState()),
    m_receiveFrameFromSensor    (new QState()),
    m_insertFrameIntoEntryModel (new QState()),
    m_stopReceivingFromSensor   (new QState())
{
    m_idle                      ->addTransition(this, &ProtocolStateMachine::receivingStarted,  m_startReceivingFromSensor);
    m_startReceivingFromSensor  ->addTransition(this, &ProtocolStateMachine::sensorOk,          m_receiveFrameFromSensor);
    m_receiveFrameFromSensor    ->addTransition(this, &ProtocolStateMachine::frameReceived,     m_insertFrameIntoEntryModel);
    m_receiveFrameFromSensor    ->addTransition(this, &ProtocolStateMachine::receivingStopped,  m_stopReceivingFromSensor);
    m_stopReceivingFromSensor   ->addTransition(this, &ProtocolStateMachine::sensorOk,          m_idle);
    m_insertFrameIntoEntryModel ->addTransition(this, &ProtocolStateMachine::frameInserted,     m_receiveFrameFromSensor);

    connect(m_startReceivingFromSensor, &QState::entered, this, &ProtocolStateMachine::onStartReceivingFromSensorEntered);
    connect(m_stopReceivingFromSensor,  &QState::entered, this, &ProtocolStateMachine::onStopReceivingFromSensorEntered);
    connect(m_receiveFrameFromSensor,   &QState::entered, this, &ProtocolStateMachine::onReceiveFrameFromSensorEntered);
    connect(m_insertFrameIntoEntryModel,&QState::entered, this, &ProtocolStateMachine::onInsertFrameIntoEntryModelEntered);

    connect(m_sensorClient, &ConnectionClient::sensorMessage,
            this, &ProtocolStateMachine::onIncomingSensorMessage);

    m_stateMachine.addState(m_idle);
    m_stateMachine.addState(m_startReceivingFromSensor);
    m_stateMachine.addState(m_receiveFrameFromSensor);
    m_stateMachine.addState(m_stopReceivingFromSensor);
    m_stateMachine.addState(m_insertFrameIntoEntryModel);

    m_stateMachine.setInitialState(m_idle);
    m_stateMachine.start();
}

ProtocolStateMachine::ProtocolStateMachine(QObject *parent)
    :
    ProtocolStateMachine(nullptr, nullptr, parent)
{

}

ProtocolStateMachine::~ProtocolStateMachine()
{
    delete m_idle;
    delete m_startReceivingFromSensor;
    delete m_stopReceivingFromSensor;
    delete m_receiveFrameFromSensor;
    delete m_insertFrameIntoEntryModel;
}

void ProtocolStateMachine::onIdleEntered()
{
    DebugLog << "onIdleEntered";
}

void ProtocolStateMachine::onIdleExited()
{
    DebugLog << "onIdleExited";
}

void ProtocolStateMachine::onStartReceivingFromSensorEntered()
{
    DebugLog << "Sending response: StartSending";
    m_sensorClient->sendResponse(ClientResponse::StartSendingToPc);
}

void ProtocolStateMachine::onStopReceivingFromSensorEntered()
{
    DebugLog << "Sending response: StopSending";
    m_sensorClient->sendResponse(ClientResponse::StopSendingToPc);
}

void ProtocolStateMachine::onReceiveFrameFromSensorEntered()
{
    DebugLog << "Waiting for frame to fetch...";
}

void ProtocolStateMachine::onInsertFrameIntoEntryModelEntered()
{
    DebugLog << "Inserting frame into model...";
    auto parsedMessage = MessageParser::parse(m_currentlyReceivedFrame);
    delete m_currentlyReceivedFrame;

    ModelContentCreator::Instance()
        .frame
        .type(static_cast<FrameType>(parsedMessage->argument[0].convert()))
        .values(
            std::begin  (parsedMessage->argument)+1,
            std::end    (parsedMessage->argument)
         )
        .create();

    emit frameInserted();
}

void ProtocolStateMachine::onIncomingSensorMessage(IncomingMessage *incomingMessage)
{
    auto messageType = MessageParser::type(incomingMessage);

    switch (messageType)
    {
    case SensorResponse::Ok:
        emit sensorResponse(SensorResponse::Ok);
        emit sensorOk();
        break;

    case SensorResponse::Nok:
        emit sensorResponse(SensorResponse::Nok);
        emit sensorNok();
        break;

    case SensorResponse::Frame:
        m_currentlyReceivedFrame = incomingMessage;
        emit frameReceived();
        break;
    }
}
