#include "entrysets.h"

using namespace SimulationLib;

const QVector<FieldType>EntrySets::OneSensor =
{
    FieldType::AngleField,
    FieldType::DistanceField
};

const QVector<FieldType>EntrySets::TwoSensor =
{
    FieldType::DistanceField,
    FieldType::DistanceField
};

const EntrySet &EntrySets::toEntrySet(const FrameType& type)
{
    switch (type)
    {
    case FrameType::OneSensor:   return OneSensor;
    case FrameType::TwoSensor:   return TwoSensor;
    default:                     return OneSensor;
    }
}
