#include "chartseriesmodel.h"
#include <QString>
#include <QPair>

using namespace SimulationLib;

enum DescriptionSizes
{
    OneSensor = 1,
    TwoSensor = 4,
    FourSensor = 4,
    EightSensor = 8
};

static QPair<QString, bool> OneSensorDescriptions[] =
{
    {"Sensor", true}
};

static QPair<QString, bool> TwoSensorDescriptions[] =
{
    {"Side sensor", true},
    {"Side sensor (filtered)", true},
    {"Front sensor", true},
    {"Front sensor (filtered)", true}
};

static QPair<QString, bool> FourSensorDescriptions[] =
{
    {"Sensor 1", true},
    {"Sensor 2", true},
    {"Sensor 3", true},
    {"Sensor 4", true}
};

static QPair<QString, bool> EightSensorDescriptions[] =
{
    {"Sensor 1", true},
    {"Sensor 2", true},
    {"Sensor 3", true},
    {"Sensor 4", true},
    {"Sensor 5", true},
    {"Sensor 6", true},
    {"Sensor 7", true},
    {"Sensor 8", true}
};

QString ChartSeriesModel::getSeriesDescription(SimulationLib::FrameType frameType, int index)
{
    switch (frameType)
    {
    case FrameType::OneSensor:
        if (index >= 0 && index < DescriptionSizes::OneSensor) {
            return OneSensorDescriptions[index].first;
        }
        break;
    case FrameType::TwoSensor:
        if (index >= 0 && index < DescriptionSizes::TwoSensor) {
            return TwoSensorDescriptions[index].first;
        }
        break;
    case FrameType::FourSensor:
        if (index >= 0 && index < DescriptionSizes::FourSensor) {
            return FourSensorDescriptions[index].first;
        }
        break;
    case FrameType::EightSensor:
        if (index >= 0 && index < DescriptionSizes::EightSensor) {
            return EightSensorDescriptions[index].first;
        }
        break;
    }

    return QString("");
}

void ChartSeriesModel::setSeriesVisibility(SimulationLib::FrameType frameType, int index, bool visibility)
{
    switch (frameType)
    {
    case FrameType::OneSensor:
        if (index >= 0 && index < DescriptionSizes::OneSensor) {
            OneSensorDescriptions[index].second = visibility;
        }
        break;
    case FrameType::TwoSensor:
        if (index >= 0 && index < DescriptionSizes::TwoSensor) {
            TwoSensorDescriptions[index].second = visibility;
        }
        break;
    case FrameType::FourSensor:
        if (index >= 0 && index < DescriptionSizes::FourSensor) {
            FourSensorDescriptions[index].second = visibility;
        }
        break;
    case FrameType::EightSensor:
        if (index >= 0 && index < DescriptionSizes::EightSensor) {
            EightSensorDescriptions[index].second = visibility;
        }
        break;
    }
}

bool ChartSeriesModel::getSeriesVisibility(SimulationLib::FrameType frameType, int index)
{
    switch (frameType)
    {
    case FrameType::OneSensor:
        if (index >= 0 && index < DescriptionSizes::OneSensor) {
            return OneSensorDescriptions[index].second;
        }
        break;
    case FrameType::TwoSensor:
        if (index >= 0 && index < DescriptionSizes::TwoSensor) {
            return TwoSensorDescriptions[index].second;
        }
        break;
    case FrameType::FourSensor:
        if (index >= 0 && index < DescriptionSizes::FourSensor) {
            return FourSensorDescriptions[index].second;
        }
        break;
    case FrameType::EightSensor:
        if (index >= 0 && index < DescriptionSizes::EightSensor) {
            return EightSensorDescriptions[index].second;
        }
        break;
    }
    return false;
}
