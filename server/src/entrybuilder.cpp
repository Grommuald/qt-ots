#include "entrybuilder.h"
#include "entrysets.h"

#include "sl_qmlutils.h"
using namespace SimulationLib;

FrameBuilder::FrameBuilder ()
{
    obj = new Frame();
}

FrameBuilder& FrameBuilder::type (const FrameType& frameType)
{
    auto currentEntrySet = EntrySets::toEntrySet(frameType);
    obj->setNumberOfFields(currentEntrySet.count());
    obj->instantiateFields(currentEntrySet);

    return *this;
}

FrameBuilder& FrameBuilder::values (
        QVector<SimulationLib::Argument>::iterator begin,
        QVector<SimulationLib::Argument>::iterator end)
{
    int numericIndex = 0;
    for (auto i = begin; i != end; ++i) {
        obj->updateContent(numericIndex++, i->convert());
    }

    return *this;
}
