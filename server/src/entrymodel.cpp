#include "entrymodel.h"
#include "simpletypes.h"

#include "sl_debug.h"

EntryModel::EntryModel(QObject *parent)
    : QAbstractListModel(parent)
{

}

EntryModel::~EntryModel()
{
    qDeleteAll(m_model);
}

bool EntryModel::addEntry(Entry* entry)
{
    m_currentlyAddedEntry = entry;
    return insertRows(m_model.count(), 1);
}

bool EntryModel::addEntry(Entry* entry, int row)
{
    m_currentlyAddedEntry = entry;
    return insertRows(row, 1);
}

bool EntryModel::clearAll()
{
    return removeRows(0, m_model.count());
}

bool EntryModel::removeEntry(int row)
{
    return removeRows(row, 1);
}

Entry *EntryModel::getEntry(int idNumber)
{
    if (idNumber < 0) {
        return nullptr;
    }

    return m_model[idNumber];
}

int EntryModel::size() const
{
    return rowCount(QModelIndex());
}

int EntryModel::rowCount(const QModelIndex& index) const
{
    Q_UNUSED(index);
    return m_model.count();
}

QVariant EntryModel::data(const QModelIndex& index, int role) const
{
    Entry* entry = m_model[index.row()];

    switch (role) {
    case NumberOfFieldsRole:
        return entry->numberOfFields();
    case IdRole:
        return entry->id();
    case FieldsRole:
        return QVariant::fromValue(entry->fields());
    }
    return QVariant();
}

void EntryModel::update()
{
    auto start_index = createIndex(0, 0);
    auto end_index = createIndex(m_model.count() - 1, 0);

    emit dataChanged(start_index, end_index);
}

QHash<int, QByteArray> EntryModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NumberOfFieldsRole]   = "numberOfFields";
    roles[IdRole]               = "entryId";
    roles[FieldsRole]           = "fields";

    return roles;
}

bool EntryModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    Q_UNUSED(count);

    if (row < 0) {
        row = 0;
    }
    else if (row >= m_model.count()) {
        row = m_model.count();
    }

    m_currentlyAddedEntry->setId(row);
    for (int i = row; i < m_model.count(); ++i) {
        m_model[i]->setId(i+1);
    }

    emit insertion(row, m_currentlyAddedEntry->convertToSimpleEntryPtr());

    beginInsertRows(QModelIndex(), row, row);
    m_model.insert(row, m_currentlyAddedEntry);
    endInsertRows();

    update();

    return true;
}

bool EntryModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);

    if (row < 0 || row >= m_model.count()) {
        return false;
    }

    auto numberOfElements = row + count - 1;

    beginRemoveRows(QModelIndex(), row, numberOfElements);
    for (int i = row; i <= numberOfElements; ++i) {
        // always take the first one and delete it
        // as all the consecutive elements shift left after takeAt operation
        emit removal(row);
        delete m_model.takeAt(row);
    }
    endRemoveRows();

    if (m_model.count() > 0) {
        // apply id changes to the entries that were after row
        for (int i = row; i < m_model.count(); ++i) {
            m_model[i]->setId(i);
        }
    }
    DebugLog << "Model count after clear: " << m_model.count();
    update();

    return true;
}
