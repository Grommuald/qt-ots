#include "fieldfactory.h"
#include "fieldbuilder.h"
using namespace SimulationLib;

Field* FieldFactory::createField(const FieldType &type)
{
    switch (type)
    {
    case FieldType::AngleField:
        return createAngleField();
    case FieldType::BooleanField:
        return createBooleanField();
    case FieldType::DistanceField:
        return createDistanceField();
    case FieldType::TimeField:
        return createTimeField();
    case FieldType::VelocityField:
        return createVelocityField();
    }
    Q_UNREACHABLE();
}

Field* FieldFactory::createAngleField()
{
    return FieldBuilder()
            .type(FieldType::AngleField)
            .content(0)
            .build();
}

Field* FieldFactory::createBooleanField()
{
    return FieldBuilder()
            .type(FieldType::BooleanField)
            .content(0)
            .build();
}

Field* FieldFactory::createDistanceField()
{
    return FieldBuilder()
            .type(FieldType::DistanceField)
            .content(0)
            .build();
}

Field* FieldFactory::createVelocityField()
{
    return FieldBuilder()
            .type(FieldType::VelocityField)
            .content(0)
            .build();
}

Field* FieldFactory::createTimeField()
{
    return FieldBuilder()
            .type(FieldType::TimeField)
            .content(0)
            .build();
}
