#include "fieldbuilder.h"
using namespace SimulationLib;

FieldBuilder::FieldBuilder()
{
    instance = new Field();
}

FieldBuilder &FieldBuilder::type(const FieldType &value)
{
    instance->setType(value);
    return *this;
}

FieldBuilder &FieldBuilder::content(int value)
{
    instance->setContent(value);
    return *this;
}

Field* FieldBuilder::build()
{
    Field* result = instance;
    instance = new Field();

    return result;
}
