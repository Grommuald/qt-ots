#include "field.h"
#include "sl_protocolcommands.h"
using namespace SimulationLib;

#include <QMap>

namespace
{
    QMap<FieldType, ValueType> TypeSizes =
    {
        {FieldType::DistanceField,  ValueType::UInt16},
        {FieldType::VelocityField,  ValueType::UInt16},
        {FieldType::TimeField,      ValueType::UInt16},
        {FieldType::AngleField,     ValueType::SInt16},
        {FieldType::BooleanField,   ValueType::UInt8}
    };
}

Field::Field(QObject *parent)
    : Field(FieldType::DistanceField, parent)
{

}

Field::Field(const FieldType &type, QObject *parent)
    : QObject(parent),
      m_type(type)
{

}

int Field::content() const
{
    return m_content;
}

void Field::setContent(int content)
{
    m_content = content;
    emit contentChanged();
}

void Field::setType(const FieldType &type)
{
    m_type = type;
}

SimpleField Field::get() const
{
    SimpleField result;

    result.size = TypeSizes[m_type];
    result.value = m_content;

    return result;
}
