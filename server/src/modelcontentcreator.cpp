#include "modelcontentcreator.h"

ModelContentCreator::ModelContentCreator()
{

}

ModelContentCreator::~ModelContentCreator()
{

}

ModelContentCreator &ModelContentCreator::Instance()
{
    static ModelContentCreator instance;
    return instance;
}

void ModelContentCreator::setContext(EntryModel *context)
{
    m_entryModelContext = context;
    frame.setModel(m_entryModelContext);
}
