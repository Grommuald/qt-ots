#include "chartmodel.h"
#include "chartseriesmodel.h"
#include "sl_debug.h"
using namespace SimulationLib;

ChartModel::ChartModel(EntryModel *entryModel, SimulationModel* simulationModel, QObject *parent)
    : QObject(parent), m_entryModel(entryModel), m_simulationModel(simulationModel)
{
    m_filters.emplace_back(10);
    m_filters.emplace_back(10);

    connect(m_entryModel, &EntryModel::insertion,
            this, &ChartModel::onModelInsertion);
    connect(m_entryModel, &EntryModel::removal,
            this, &ChartModel::onModelRemoval);
}

ChartModel::ChartModel(QObject *parent)
    : ChartModel(nullptr, nullptr, parent)
{

}

QString ChartModel::getSeriesDescription(int index)
{
    return ChartSeriesModel::getSeriesDescription(m_simulationModel->frameType(), index);
}

void ChartModel::setSeriesVisibility(int index, bool checked)
{
    ChartSeriesModel::setSeriesVisibility(m_simulationModel->frameType(), index, checked);
    emit seriesVisibilityChanged(index, checked);
}

bool ChartModel::getSeriesVisibility(int index)
{
    return ChartSeriesModel::getSeriesVisibility(m_simulationModel->frameType(), index);
}

int ChartModel::makeScreenshot()
{
    emit screenshotMade(m_screenshotNumber);
    return m_screenshotNumber++;
}

void ChartModel::onModelInsertion(int index, SimpleEntry* entry)
{
    QList<int> coordPackage;
    auto filterIndex = 0U;

    DebugLog << "onModelInsertion -- begin";
    for (const auto& i : entry->values) {
        auto currentValue = i.value;
        coordPackage.push_back(currentValue);
        // also add the filtered versions of values
        coordPackage.push_back(m_filters[filterIndex++].updateQueue(currentValue).getCurrentDominantValue());
    }
    DebugLog << "onModelInsertion -- end";
    delete entry;
    emit insert(index, coordPackage);
}

void ChartModel::onModelRemoval(int index)
{
    emit remove(index);
}
