#include "sl_connectionstatus.h"

#include "simulationmodel.h"
#include "protocolstatemachine.h"
#include "entrymodel.h"
#include "readerclient.h"
#include "modelcontentcreator.h"
#include "chartmodel.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQuickStyle::setStyle("Material");

    qmlRegisterUncreatableType<SimulationLib::ConnectionStatus>(
        "qt.mock.server", 1, 0, "ConnectionStatus",
        "Cannot create ConnectionStatus in QML"
    );

    qmlRegisterType<Entry>();
    qmlRegisterType<Field>();
    qmlRegisterType<Frame>("test.entry", 1, 0, "Frame");

    QQmlApplicationEngine engine;

    EntryModel entryModel;
    engine.rootContext()->setContextProperty("EntryModel", &entryModel);

    SimulationModel simulationModel;

    ChartModel chartModel(&entryModel, &simulationModel);
    engine.rootContext()->setContextProperty("ChartModel", &chartModel);

    ReaderClient sensor;
    engine.rootContext()->setContextProperty("Sensor", &sensor);

    ProtocolStateMachine psm (&entryModel, &sensor);
    engine.rootContext()->setContextProperty("ProtocolStateMachine", &psm);

    ModelContentCreator::Instance().setContext(&entryModel);

    engine.load(QUrl::fromLocalFile("main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
