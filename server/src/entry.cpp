#include "fieldfactory.h"
#include "entry.h"
#include "sl_debug.h"
using namespace SimulationLib;

#include <QVector>

using EntrySet = const QVector<FieldType>;

Entry::Entry(QObject *parent)
    : Entry(EntryType::Frame, parent)
{

}

Entry::Entry(const EntryType &type, QObject *parent)
    : QObject(parent),
      m_type(type),
      m_numberOfFields(0)
{

}

Entry::~Entry()
{

}

bool Entry::updateContent(int id, const QString &newContent)
{
    return updateContent(id, newContent.toInt());
}

bool Entry::updateContent(int id, const int& newContent)
{
    auto field = getField(id);

    if (field == nullptr) {
        return false;
    }
    qobject_cast<Field*>(field)->setContent(newContent);
    return true;
}

Field *Entry::getField(int id)
{
    DebugLog << "m_idCount.count(): " << m_dataList.count() << "; Field id: " << id;

    if (id < 0 || id >= m_dataList.count()) {
        DebugLog << "Cannot get content...";
        return nullptr;
    }
    return qobject_cast<Field*>(m_dataList[id]);
}

void Entry::instantiateFields(const EntrySet& set)
{
    DebugLog << "EntrySet length: " << set.count();

    for(auto i = 0; i < set.count(); ++i) {
        m_dataList.append(FieldFactory::createField(set[i]));
        emit fieldsChanged();
    }
}

quint8 Entry::numberOfFields() const
{
    return m_numberOfFields;
}

void Entry::setNumberOfFields(const quint8 &numberOfFields)
{
    m_numberOfFields = numberOfFields;
}

int Entry::id() const
{
    return m_id;
}

void Entry::setId(int id)
{
    m_id = id;
    emit idSet();
}

void Entry::setNewId()
{
    static int CurrentIdNumber = 0;
    setId(CurrentIdNumber++);
}

SimpleEntry Entry::convertToSimpleEntryValue() const
{
    SimpleEntry result;

    for (int index = 0; index < m_dataList.count(); ++index) {
        result.values.push_back(qobject_cast<Field*>(m_dataList[index])->get());
    }
    result.type = m_type;
    return result;
}

SimpleEntry* Entry::convertToSimpleEntryPtr() const
{
    SimpleEntry* result = new SimpleEntry(convertToSimpleEntryValue());
    return result;
}

Frame::Frame(QObject* parent)
    : Entry(parent)
{

}

Frame::Frame(const FrameType& frameType, QObject* parent)
    : Entry(EntryType::Frame, parent),
      m_subtype(frameType)
{

}

SimulationLib::FrameType Frame::subtype() const
{
    return m_subtype;
}

void Frame::setSubtype(const FrameType &subtype)
{
    m_subtype = subtype;
}
