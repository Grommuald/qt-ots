#include "readerclient.h"
using namespace SimulationLib;

ReaderClient::ReaderClient(QObject* parent)
    : ConnectionClient(parent)
{

}

void ReaderClient::onConnectedToServer()
{
    sendResponse(ClientResponse::SayHiAsReader);
}
