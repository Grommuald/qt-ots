#ifndef PROTOCOLSTATEMACHINE_H
#define PROTOCOLSTATEMACHINE_H

#include "sl_protocolcommands.h"
#include "sl_argument.h"
#include "sl_connectionstatus.h"
#include "sl_connectionclient.h"
#include "sl_message.h"
using SimulationLib::ConnectionStatus;

#include "simulationmodel.h"
#include "protocolstatemachine.h"
#include "entrymodel.h"

#include <QObject>
#include <QStateMachine>
#include <QVector>

class  EntryModel;

class ProtocolStateMachine : public QObject
{
    Q_OBJECT

public:
    explicit ProtocolStateMachine (
        EntryModel*  entryModel,
        SimulationLib::ConnectionClient*  sensorClient,
        QObject *parent = nullptr
    );
    explicit ProtocolStateMachine (QObject *parent = nullptr);
    ~ProtocolStateMachine();

signals:
    void frameReceived();
    void frameInserted();
    void receivingStopped();
    void receivingStarted();
    void runningStatusChanged(bool val);

    void sensorResponse(SimulationLib::SensorResponse response);
    void sensorOk();
    void sensorNok();

private slots:
    void onStartReceivingFromSensorEntered();
    void onStopReceivingFromSensorEntered();
    void onReceiveFrameFromSensorEntered();
    void onInsertFrameIntoEntryModelEntered();
    void onIdleEntered();
    void onIdleExited();

    void onIncomingSensorMessage(SimulationLib::IncomingMessage* incomingFrame);

private:
    SimulationLib::ConnectionClient* m_sensorClient;
    EntryModel* m_entryModel;

    SimulationLib::IncomingMessage* m_currentlyReceivedFrame;

    QStateMachine m_stateMachine;

    QState* m_idle;
    QState* m_startReceivingFromSensor;
    QState* m_receiveFrameFromSensor;
    QState* m_insertFrameIntoEntryModel;
    QState* m_stopReceivingFromSensor;
};

#endif // PROTOCOLSTATEMACHINE_H
