#ifndef CHARTSERIESMODEL_H
#define CHARTSERIESMODEL_H

#include "sl_types.h"
#include <QString>

struct ChartSeriesModel
{
    static QString getSeriesDescription(
        SimulationLib::FrameType frameType,
        int index
    );
    static void setSeriesVisibility(
        SimulationLib::FrameType frameType,
        int index,
        bool visibility
    );
    static bool getSeriesVisibility(
        SimulationLib::FrameType frameType,
        int index
    );
};

#endif
