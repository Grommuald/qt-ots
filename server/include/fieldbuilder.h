#ifndef FIELDBUILDER_H
#define FIELDBUILDER_H

#include "field.h"

class FieldBuilder
{
public:
    FieldBuilder();

    FieldBuilder& type(const SimulationLib::FieldType& value);
    FieldBuilder& content(int value);

    Field* build();

private:
    Field* instance;
};

#endif // FIELDBUILDER_H
