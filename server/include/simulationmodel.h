#ifndef SIMULATIONMODEL_H
#define SIMULATIONMODEL_H

#include "sl_types.h"

class SimulationModel
{
public:
    SimulationLib::FrameType frameType() const;
private:
    SimulationLib::FrameType m_frameType {SimulationLib::FrameType::TwoSensor};
};

#endif // SIMULATIONMODEL_H
