#ifndef CHARTMODEL_H
#define CHARTMODEL_H

#include "sl_dominantqueue.h"
#include "entrymodel.h"
#include "simulationmodel.h"

#include <QList>

class ChartModel : public QObject
{
    Q_OBJECT
public:
    explicit ChartModel(EntryModel* entryModel,
                        SimulationModel* simulationModel,
                        QObject* parent = nullptr);
    explicit ChartModel(QObject* parent = nullptr);

    Q_INVOKABLE QString getSeriesDescription(int index);
    Q_INVOKABLE void    setSeriesVisibility(int index, bool checked);
    Q_INVOKABLE bool    getSeriesVisibility(int index);
    Q_INVOKABLE int     makeScreenshot();

signals:
    void insert(int index, QList<int> valueSet);
    void remove(int index);
    void seriesVisibilityChanged(int index, bool value);
    void screenshotMade(int screenshotNumber);
    void clear();

public slots:
    void onModelInsertion(int index, SimpleEntry* entry);
    void onModelRemoval(int index);

private:
    EntryModel* m_entryModel;
    SimulationModel* m_simulationModel;
    std::vector<SimulationLib::DominantQueue> m_filters;
    int m_screenshotNumber { 0 };
};
#endif
