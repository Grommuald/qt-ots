#ifndef ENTRY_H
#define ENTRY_H

#include <QObject>
#include <QVector>
#include <QMap>

#include "field.h"
#include "sl_types.h"

class Entry : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int entryId READ id NOTIFY idSet)
    Q_PROPERTY(quint8 numberOfFields READ numberOfFields)

public:
    explicit Entry(QObject *parent = nullptr);
    explicit Entry(const SimulationLib::EntryType& type, QObject *parent = nullptr);
    virtual ~Entry();

    Q_INVOKABLE bool updateContent(int id, const QString& newContent);
                bool updateContent(int id, const int&     newContent);
    Q_INVOKABLE Field* getField(int id);

    void instantiateFields(const QVector<SimulationLib::FieldType>& set);

    QList<QObject*> fields() const { return m_dataList; }

    quint8 numberOfFields() const;
    void setNumberOfFields(const quint8 &numberOfFields);

    int id() const;
    void setId(int id);
    void setNewId();

    SimpleEntry  convertToSimpleEntryValue() const;
    SimpleEntry* convertToSimpleEntryPtr()   const;

signals:
    void idSet();
    void fieldsChanged();

protected:
    QList<QObject*> m_dataList;
    int m_id;
    SimulationLib::EntryType m_type;
    quint8 m_numberOfFields;
};

class Frame : public Entry
{
    Q_OBJECT

public:
    explicit Frame(QObject* parent = nullptr);
    explicit Frame(const SimulationLib::FrameType& type, QObject* parent = nullptr);

    SimulationLib::FrameType subtype() const;
    void setSubtype(const SimulationLib::FrameType &subtype);

private:
    SimulationLib::FrameType m_subtype;
};

#endif // ENTRY_H
