#ifndef MODELCONTENTCREATOR_H
#define MODELCONTENTCREATOR_H

#include "entrybuilder.h"

struct ModelContentCreator
{
    static ModelContentCreator& Instance();

    FrameBuilder    frame;

    ModelContentCreator(ModelContentCreator const&)             = delete;
    ModelContentCreator(ModelContentCreator&&)                  = delete;
    ModelContentCreator& operator= (ModelContentCreator const&) = delete;
    ModelContentCreator& operator= (ModelContentCreator&&)      = delete;

    void setContext(EntryModel* context);

private:
    EntryModel* m_entryModelContext;

    ModelContentCreator();
    ~ModelContentCreator();
};

#endif
