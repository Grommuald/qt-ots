#ifndef ENTRYSETS_H
#define ENTRYSETS_H

#include "sl_types.h"
#include <QVector>

using EntrySet = const QVector<SimulationLib::FieldType>;

class EntrySets
{
public:
    static EntrySet OneSensor;
    static EntrySet TwoSensor;

    static const EntrySet& toEntrySet(const SimulationLib::FrameType& type);
};

#endif // ENTRYSETS_H
