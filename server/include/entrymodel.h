#ifndef ENTRYMODEL_H
#define ENTRYMODEL_H

#include <QObject>
#include <QVector>
#include <QHash>
#include <QQmlListProperty>
#include <QAbstractListModel>

#include "entry.h"

class EntryModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int size READ size NOTIFY resized)

public:
    enum EntryRoles {
        NumberOfFieldsRole = Qt::UserRole + 1,
        IdRole,
        FieldsRole
    };

    explicit EntryModel(QObject* parent = nullptr);
    ~EntryModel() override;

    bool addEntry(Entry* entry);
    bool addEntry(Entry* entry, int row);

    Q_INVOKABLE bool clearAll();
    Q_INVOKABLE bool removeEntry(int row);

    Q_INVOKABLE Entry* getEntry(int idNumber);

    QVariant data(const QModelIndex& index, int role) const override;
    int size() const;

signals:
    void resized();
    void insertion(int index, SimpleEntry* entry);
    void removal(int index);

private:
    QList<Entry*> m_model;
    Entry* m_currentlyAddedEntry;

    void update();

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex&) const override;
    bool insertRows(int row,
                    int count,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int row,
                    int count,
                    const QModelIndex& parent = QModelIndex()) override;
};

#endif // ENTRYMODEL_H
