#ifndef SIMPLETYPES_H
#define SIMPLETYPES_H

#include "sl_types.h"
#include <QVector>

struct SimpleField
{
    SimulationLib::ValueType size;
    int value;
};

struct SimpleEntry
{
    SimulationLib::EntryType type;
    QVector<SimpleField> values;
};

#endif
