#ifndef ENTRYBUILDER_H
#define ENTRYBUILDER_H

#include "entrymodel.h"

#include "sl_debug.h"
#include "sl_argument.h"
#include "sl_types.h"

#include <QVector>

template <typename T>
struct Builder
{
    void setModel(EntryModel* entryModel)
    {
        m_entryModelContext = entryModel;
    }
    void create()
    {
        m_entryModelContext->addEntry(std::move(build()));
    }
    void create(int row)
    {
        m_entryModelContext->addEntry(std::move(build()), row);
    }
    T* build()
    {
        T* result = obj;
        obj = new T();
        return result;
    }

protected:
    Builder() {}
    EntryModel* m_entryModelContext;
    T* obj;
};

struct FrameBuilder : public Builder<Frame>
{
public:
    FrameBuilder& type   (const SimulationLib::FrameType& frameType);
    FrameBuilder& values (QVector<SimulationLib::Argument>::iterator begin,
                          QVector<SimulationLib::Argument>::iterator end);
    FrameBuilder();
};
#endif // ENTRYBUILDER_H
