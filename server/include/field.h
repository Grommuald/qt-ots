#ifndef FIELD_H
#define FIELD_H

#include "sl_types.h"
#include "simpletypes.h"

#include <QObject>

class Field : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int content READ content WRITE setContent NOTIFY contentChanged)

public:
    explicit Field(QObject* parent = nullptr);
    explicit Field(const SimulationLib::FieldType& type, QObject* parent = nullptr);

    int content() const;
    void setContent(int content);

    SimulationLib::FieldType type() const;
    void setType(const SimulationLib::FieldType &type);

    SimpleField get() const;

signals:
    void contentChanged();

private:
    int m_content;
    SimulationLib::FieldType m_type;
};
#endif // FIELD_H
