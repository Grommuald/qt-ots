#ifndef READERCLIENT_H
#define READERCLIENT_H

#include "sl_connectionclient.h"

class ReaderClient : public SimulationLib::ConnectionClient
{
    Q_OBJECT

public:
    explicit ReaderClient(QObject* parent = nullptr);
    void onConnectedToServer() final;
};

#endif
