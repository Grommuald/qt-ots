#ifndef FIELDFACTORY_H
#define FIELDFACTORY_H

#include "fieldbuilder.h"

class FieldFactory
{
public:
    static Field* createField(const SimulationLib::FieldType& type);

    static Field* createAngleField();
    static Field* createBooleanField();
    static Field* createDistanceField();
    static Field* createVelocityField();
    static Field* createTimeField();
};
#endif // FIELDFACTORY_H
