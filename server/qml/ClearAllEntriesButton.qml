import QtQuick 2.9
import QtQuick.Controls 2.4

Button {
    width: 80
    text: qsTr("Clear")

    onClicked: {
        EntryModel.clearAll();
        ChartModel.clear();
    }
}
