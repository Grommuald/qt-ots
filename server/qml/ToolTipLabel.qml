import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
    id: toolTipLabel

    property alias text: label.text
    property alias hintText: toolTip.text

    width: 300
    height: 40

    Label {
        id: label
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
    }

    MouseArea {
        id: mouseArea
        anchors.fill: label
        hoverEnabled: true
    }

    ToolTip {
        id: toolTip
        visible: mouseArea.containsMouse
        x: parent.x
        y: parent.height

        delay: 1000
        timeout: 5000
    }
}
