import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
    id: inputSequence
    property bool autoScrollEnabled: false

    ListView {
        property int popupMenuButtonRightMargin: 24
        property int delegateHighlightRectRightMargin: 16

        id: mainList
        model: EntryModel
        anchors {
            top: parent.top;
            left: parent.left
            right: parent.right
            bottom: buttonsBar.top
        }
        height: parent.height
        spacing: 5
        highlightFollowsCurrentItem: true
        interactive: autoScrollEnabled
        clip: true

        delegate: Item {
            id: item
            width: mainList.width
            height: 40

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                Rectangle {
                    id: highlightRect
                    anchors { top: parent.top; left: parent.left; right: parent.right; bottom: parent.bottom; rightMargin: mainList.delegateHighlightRectRightMargin }
                    opacity: 0.0
                    color: "#4286f4"
                    radius: 5.0
                }

                Item {
                    id: entryNumberLabel

                    width: 32
                    height: 32
                    anchors { left: parent.left; verticalCenter: parent.verticalCenter }

                    Label {
                        anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
                        font.pixelSize: 10
                        text: entryId
                    }
                }

                Image {
                    id: entryImage
                    anchors { left: entryNumberLabel.right; top: parent.top; verticalCenter: parent.verticalCenter }

                    width: 32
                    height: 32
                    fillMode: Image.PreserveAspectFit

                    source: "media/icons/frame.png"
                }

                ListView {
                    id: fieldList
                    height: 40
                    model: fields
                    orientation: ListView.Horizontal
                    anchors { left: entryImage.right; right: parent.right }
                    spacing: 5

                    delegate: EntryField {
                        anchors { verticalCenter: parent.verticalCenter }
                        text: model.modelData.content
                        parentId: entryId
                        fieldId: index
                    }
                }

                onEntered: {
                    highlightRect.opacity = 0.3;
                }
                onExited: {
                    highlightRect.opacity = 0.0;
                }
            }
        }

        ScrollIndicator.vertical: ScrollIndicator {
            id: scrollIndicator
            enabled: true
        }

        onCountChanged: {
            scrollViewToEnd();
        }

        states: [
            State {
                name: "scrollIndicatorActive"
                when: scrollIndicator.active
                PropertyChanges {
                    target: mainList
                    popupMenuButtonRightMargin: 24
                    delegateHighlightRectRightMargin: 16
                }
            },
            State {
                name: "scrollIndicatorInactive"
                when: !scrollIndicator.active
                PropertyChanges {
                    target: mainList
                    popupMenuButtonRightMargin: 12
                    delegateHighlightRectRightMargin: 4
                }
            }
        ]

        transitions: [
            Transition {
                to: "scrollIndicatorInactive"

                SequentialAnimation {
                    PauseAnimation {
                        duration: 500
                    }

                    ParallelAnimation {
                        NumberAnimation {
                            target: mainList
                            property: "popupMenuButtonRightMargin"
                            duration: 200
                            easing.type: Easing.InOutQuad
                        }
                        NumberAnimation {
                            target: mainList
                            property: "delegateHighlightRectRightMargin"
                            duration: 200
                            easing.type: Easing.InOutQuad
                        }
                    }
                }
            },
            Transition {
                to: "scrollIndicatorActive"

                SequentialAnimation {
                    ParallelAnimation {
                        NumberAnimation {
                            target: mainList
                            property: "popupMenuButtonRightMargin"
                            duration: 200
                            easing.type: Easing.InOutQuad
                        }
                        NumberAnimation {
                            target: mainList
                            property: "delegateHighlightRectRightMargin"
                            duration: 200
                            easing.type: Easing.InOutQuad
                        }
                    }
                }
            }
        ]

        function scrollViewToEnd() {
           var newIndex = mainList.count - 1;
           mainList.positionViewAtEnd();
           mainList.currentIndex = newIndex;
        }
    }

    Item {
        id: buttonsBar
        anchors { bottom: parent.bottom; left: parent.left; right: parent.right }
        height: 50

        ClearAllEntriesButton {
            id: clearEntriesButton
            anchors { top: parent.top; left: parent.left; leftMargin: 10; bottom: parent.bottom }
        }

        state: "enabled"
        states: [
            State {
                name: "enabled"
                when: !sensorModeRadio.checked
                PropertyChanges {
                    target: buttonsBar
                    height: 50
                    enabled: true
                }
            },
            State {
                name: "disabled"
                when: sensorModeRadio.checked
                PropertyChanges {
                    target: buttonsBar
                    height: 0
                    enabled: false
                }
            }
        ]
        transitions: [
            Transition {
                to: "enabled"
                NumberAnimation {
                    target: buttonsBar
                    property: "height"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            },
            Transition {
                to: "disabled"
                NumberAnimation {
                    target: buttonsBar
                    property: "height"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        ]
    }
}
