import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import qt.mock.server 1.0

ApplicationWindow {
    property int windowWidth: 950
    property int windowHeight: 600

    id: serverApplicationWindow
    visible: true
    title: qsTr("Sensor Reader")

    Material.theme: Material.Dark
    Material.accent: Material.Orange

    // Unable window-resizing
    minimumWidth: windowWidth
    minimumHeight: windowHeight

    maximumWidth: minimumWidth
    maximumHeight: minimumHeight

    Connections {
        target: Sensor

        onStatusChanged: {
            refreshSimulationGroupBox();
            refreshSensorStatus();
        }
    }

    function refreshSimulationGroupBox() {
        simulationGroupBox.visible = Sensor.status === ConnectionStatus.Connected;
        unavailabilityLabel.visible = !simulationGroupBox.visible;
        simulationGroupBox.enabled = simulationGroupBox.visible;
    }

    function refreshSensorStatus() {
        sensorConnectionPortTextField.enabled = Sensor.status !== ConnectionStatus.Connected;
        sensorConnectionAddressTextField.enabled = Sensor.status !== ConnectionStatus.Connected;

        connectionShutter.buttonEnabled = simulationGroupBox.visible;

        if (Sensor.status === ConnectionStatus.Connected) {
            sensorConnectionButton.text = qsTr("Disconnect");
            sensorConnectionIndicator.state = "connected";
            chartParametersShutter.buttonEnabled = true;
        }
        else if (Sensor.status === ConnectionStatus.Disconnected
                || Sensor.status === ConnectionStatus.Failure)
        {
            chartParametersShutter.buttonEnabled = false;
            chartParametersShutter.state = "movingLeft";
            sensorConnectionButton.text = qsTr("Connect");
            sensorModeRadio.checked = false;

            if (Sensor.status === ConnectionStatus.Disconnected) {
                sensorConnectionIndicator.state = "disconnected";
            }
            else if (Sensor.status === ConnectionStatus.Failure) {
                connectionShutter.state = "movingRight";
                sensorConnectionIndicator.state = "error";
            }
        }
    }

    Component.onCompleted: {
        refreshSimulationGroupBox();
        refreshSensorStatus();
    }

    Shutter {
        id: chartParametersShutter
        visible: simulationGroupBox.visible && connectionShutter.state == "idleLeft"
        shutterButtonText: qsTr("Chart\napperance")
        shutterButtonIconImageSource: "media/images/pencil_icon.png"
        verticalDrawerButtonPosition: height * 0.5 + 25
        isShutterButtonPullable: simulationGroupBox.visible
        width: serverApplicationWindow.windowWidth * 0.35

        GroupBox {
            id: chartParametersGroupBox
            font.pixelSize: 12
            title: qsTr("Chart appearance")
            anchors { fill: parent; margins: 10 }

            GroupBox {
                title: qsTr("Data series visibility")
                anchors { left: parent.left; right: parent.right; top: parent.top }

                Column {
                    anchors { fill: parent }
                    Repeater {
                        model: 4
                        delegate: CheckBox {
                            text: ChartModel.getSeriesDescription(index)

                            onCheckedChanged: {
                                ChartModel.setSeriesVisibility(index, checked);
                            }

                            Component.onCompleted: {
                                checked = ChartModel.getSeriesVisibility(index);
                            }
                        }
                    }
                }
            }

            SequentialAnimation {
                id: labelFadeInAndOutAnimation
                PropertyAnimation {
                    target: chartSavedMessageLabel
                    properties: "opacity"
                    from: 0.0; to: 1.0
                    easing.type: Easing.InOutQuad
                    duration: 1000
                }

                PauseAnimation {
                    duration: 1000
                }

                PropertyAnimation {
                    target: chartSavedMessageLabel
                    properties: "opacity"
                    from: 1.0; to: 0.0
                    easing.type: Easing.InOutQuad
                    duration: 1000
                }
            }

            Button {
                property int    chartScreenshotNumber: 0
                property string chartScreenshotFilename

                id: screenshotButton
                anchors { left: parent.left; bottom: parent.bottom }
                width: 40
                height: 50

                Image {
                    id: saveIconImage
                    anchors { centerIn: parent }
                    fillMode: Image.PreserveAspectFit
                    width: 24
                    height: 24
                    smooth: true
                    source: "media/images/save_icon.png"
                }

                onClicked: {
                    chartScreenshotNumber = ChartModel.makeScreenshot();
                    labelFadeInAndOutAnimation.start();
                    chartScreenshotFilename = "chart" + chartScreenshotNumber + ".png";
                }

                ToolTip {
                    id: saveButtonToolTip
                    visible: parent.hovered
                    text: qsTr("Save chart to image.")
                    x: screenshotButton.width + 10
                    y: 6

                    delay: 500
                    timeout: 5000
                }
            }

            Label {
                id: chartSavedMessageLabel
                opacity: 0.0
                anchors { left: screenshotButton.right; verticalCenter: screenshotButton.verticalCenter; leftMargin: 10 }
                text: qsTr("Chart screenshot has been saved to file: " + screenshotButton.chartScreenshotFilename);
                color: "green"
            }
        }

        Component.onCompleted: {
            state = "idleLeft";
        }
    }

    Shutter {
        id: connectionShutter
        shutterButtonText: qsTr("Connection")
        shutterButtonIconImageSource: "media/images/connection_icon.png"
        isShutterButtonPullable: simulationGroupBox.visible
        width: serverApplicationWindow.windowWidth * 0.35

        GroupBox {
            id: connectionGroupBox
            title: qsTr("Connection")
            anchors { fill: parent; topMargin: 10; leftMargin: 10; rightMargin: 10; bottomMargin: 10 }

            Item {
                width: parent.width
                anchors { top: parent.top; bottom: parent.bottom }

                GroupBox {
                    id: sensorConnectionGroupBox
                    anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: 10 }
                    height: 120
                    font { pixelSize: 12 }
                    title: qsTr("Sensors")

                    Item {
                        id: sensorConnectionWidgetHolder
                        height: 40
                        anchors { top: parent.top; left: parent.left; right: parent.right }

                        ConnectionIndicator {
                            id: sensorConnectionIndicator
                            anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
                        }

                        TextField {
                            id: sensorConnectionPortTextField
                            anchors { left: sensorConnectionIndicator.right; leftMargin: 10 }
                            width: 50
                            horizontalAlignment: TextEdit.AlignHCenter
                            selectByMouse: true
                            placeholderText: qsTr("Port")
                            text: '5000'
                            inputMethodHints: Qt.ImhPreferNumbers
                            validator: RegExpValidator { regExp: /[0-9]+/ }
                            maximumLength: 4

                            onTextChanged: Sensor.port = text

                            Component.onCompleted: {
                                Sensor.port = text;
                            }
                        }

                        TextField {
                            id: sensorConnectionAddressTextField
                            anchors { left: sensorConnectionPortTextField.right; leftMargin: 10 }

                            width: 80

                            selectByMouse: true
                            placeholderText: qsTr("@Address")
                            text: '192.168.1.1'

                            onTextChanged: Sensor.address = text

                            Component.onCompleted: {
                                Sensor.address = text;
                            }
                        }

                        Button {
                            id: sensorConnectionButton
                            font { pixelSize: 12 }
                            anchors { left: sensorConnectionAddressTextField.right; right: parent.right; leftMargin: 10 }
                            height: 40
                            onClicked: {
                                Sensor.setConnection();
                            }
                        }
                    }

                    CheckBox {
                        id: sensorModeRadio
                        text: qsTr("Receive data")
                        anchors { top: sensorConnectionWidgetHolder.bottom; left: parent.left; leftMargin: -7 }

                        onCheckedChanged: {
                            if (checked) {
                                console.log("Emitting receivingStarted()...");
                                ProtocolStateMachine.receivingStarted();
                            }
                            else {
                                console.log("Emitting receivingStopped()...");
                                ProtocolStateMachine.receivingStopped();
                            }
                        }

                        states: [
                            State {
                                name: "enabled"
                                when: Sensor.status === ConnectionStatus.Connected
                                PropertyChanges {
                                    target: sensorModeRadio
                                    enabled: true
                                }
                            },
                            State {
                                name: "disabled"
                                when: Sensor.status !== ConnectionStatus.Connected
                                PropertyChanges {
                                    target: sensorModeRadio
                                    enabled: false
                                }
                            }
                        ]
                    }
                }
            }
        }
    }

    Rectangle {
        id: connectionShutterShadow
        z: 1
        anchors { top: parent.top; bottom: parent.bottom; left: parent.left; right: parent.right }
        color: "black"
        opacity: 0.5 - 0.5 * (Math.abs(connectionShutter.x) / connectionShutter.width)
    }

    Rectangle {
        id: chartShutterShadow
        z: 1
        anchors { top: parent.top; bottom: parent.bottom; left: parent.left; right: parent.right }
        color: "black"
        opacity: 0.5 - 0.5 * (Math.abs(chartParametersShutter.x) / chartParametersShutter.width)
    }

    Column {
        id: unavailabilityLabel
        anchors { verticalCenter: parent.verticalCenter; right: parent.right; rightMargin: serverApplicationWindow.width * 0.23 }
        opacity: 0.5

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Connect the sensors")
            font.pixelSize: 16
        }
        Label {
            text: qsTr("to show the data readings.")
            font.pixelSize: 16
        }
    }

    Item {
        id: simulation
        anchors { top: parent.top; bottom: parent.bottom; left: parent.left; right: parent.right }

        Item {
            id: simulationGroupBox
            anchors { top: parent.top; bottom: parent.bottom }
            width: windowWidth

            GroupBox {
                id: inputSequenceGroupBox
                anchors {
                    top: parent.top;
                    bottom: parent.bottom;
                    left: parent.left;
                    leftMargin: 15; bottomMargin: 10; topMargin: 10
                }
                implicitWidth: windowWidth * 0.3
                font { pixelSize: 12 }
                title: qsTr("Input sequence")

                InputSequence {
                    id: inputSequence
                    anchors { top: parent.top; left: parent.left; right: parent.right; bottom: parent.bottom }
                    autoScrollEnabled: !sensorModeRadio.checked
                }
            }

            GroupBox {
                id: chartGroupBox
                anchors {
                    top: parent.top; bottom: parent.bottom; left: inputSequenceGroupBox.right; right: parent.right;
                    topMargin: 10; bottomMargin: 10; leftMargin: 10; rightMargin: 10
                }
                font { pixelSize: 12 }
                title: qsTr("Data chart")

                Loader {
                    id: chartLoader
                    anchors { fill: parent }
                    source: "TwoSensorChart.qml"
                }
            }
        }
    }
}
