import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
    id: descriptedTextField

    property alias labelText: toolTipLabel.text
    property alias textFieldText: textField.text
    property alias hintText: toolTipLabel.hintText
    property alias maxLength: textField.maximumLength
    property alias textValidator: textField.validator

    signal editingFinished(int value)

    height: 40
    ToolTipLabel {
        id: toolTipLabel
        anchors {
            left: parent.left
        }
    }

    TextField {
        id: textField
        anchors {
            right: parent.right
        }

        horizontalAlignment: TextEdit.AlignHCenter
        width: 30
        height: 40

        onEditingFinished: descriptedTextField.editingFinished(parseInt(text))
    }
}
