import QtQuick 2.9
import QtCharts 2.2
import QtQuick.Controls 2.4

Item {
    property Loader chartAppearanceDrawerLoader
    property int chartScreenshotNumber
    anchors { fill: parent }

    Connections {
        target: ChartModel
        onInsert: {
            sideSensor          .insert(index, 10*index, valueSet[0]);
            sideSensorFiltered  .insert(index, 10*index, valueSet[1]);
            frontSensor         .insert(index, 10*index, valueSet[2]);
            frontSensorFiltered .insert(index, 10*index, valueSet[3]);

            timeAxis.max = 10 * EntryModel.size;
        }
        onRemove: {
            sideSensor          .remove(index);
            frontSensor         .remove(index);
            sideSensorFiltered  .remove(index);
            frontSensorFiltered .remove(index);
        }
        onSeriesVisibilityChanged: {
            if      (index == 0) { sideSensor          .visible = value; }
            else if (index == 1) { sideSensorFiltered  .visible = value; }
            else if (index == 2) { frontSensor         .visible = value; }
            else if (index == 3) { frontSensorFiltered .visible = value; }
        }
        onScreenshotMade: {
            chartScreenshotNumber = screenshotNumber;

            var filename = "chart" + chartScreenshotNumber + ".png";
            chartView.grabToImage(function(result) {
                result.saveToFile(filename);
            });
            console.log("Screenshot made! Saved to file: " + filename);
        }
        onClear: {
            timeAxis.max = 100;
        }
    }

    ChartView {
        id: chartView
        anchors { fill: parent }
        antialiasing: true
        backgroundColor: "#303030"

        legend.labelColor: "#ffffff"

        Label {
            id: timeAxisLabel
            anchors { horizontalCenter: parent.horizontalCenter; bottom: parent.bottom; bottomMargin: 15 }
            text: qsTr("Time [ms]")
            color: "#ffffff"
        }

        Label {
            id: distanceAxisLabel
            anchors { verticalCenter: parent.verticalCenter; right: parent.left; rightMargin: -80 }
            text: qsTr("Distance received [cm]")
            color: "#ffffff"
            transform: Rotation { origin.x: distanceAxisLabel.width * 0.5; origin.y: distanceAxisLabel.height * 0.5; angle: -90 }
        }

        ValueAxis {
            id: distanceAxis
            min: 0
            max: 220
            gridVisible: true
            tickCount: 10
            color: "#ffffff"
            labelsColor: "#ffffff"
            labelFormat: "%d"
        }

        ValueAxis {
            id: timeAxis
            min: 0
            max: 100
            gridVisible: true
            tickCount: 8
            color: "#ffffff"
            labelsColor: "#ffffff"
            labelFormat: "%d"
        }

        LineSeries {
            id: sideSensor
            name: ChartModel.getSeriesDescription(0)
            color : "red"

            visible: ChartModel.getSeriesVisibility(0)
            axisX: timeAxis
            axisY: distanceAxis
        }

        LineSeries {
            id: sideSensorFiltered
            name: ChartModel.getSeriesDescription(1)
            color: "yellow"

            visible: ChartModel.getSeriesVisibility(1)
            axisX: timeAxis
            axisY: distanceAxis
        }

        LineSeries {
            id: frontSensor
            name: ChartModel.getSeriesDescription(2)
            color: "blue"

            visible: ChartModel.getSeriesVisibility(2)
            axisX: timeAxis
            axisY: distanceAxis
        }

        LineSeries {
            id: frontSensorFiltered
            name: ChartModel.getSeriesDescription(3)
            color: "green"

            visible: ChartModel.getSeriesVisibility(3)
            axisX: timeAxis
            axisY: distanceAxis
        }
    }

}
