#include <ESP8266WiFi.h>
#include <vector>

struct Sensor {
  uint8_t echoPin;
  uint8_t trigPin;
};

Sensor sideSensor  = { 14, 12 };
Sensor frontSensor = { 13, 15 };

const uint8_t  FrameSize = 6;
const uint16_t DistanceMeasureIntervalInMillis = 50;

const uint16_t  Port          = 5000;

const char* ServerSsid        = "OTS-Switch";
const char* ServerPassword    = "disSrevres";

WiFiServer server(Port);
WiFiClient pcClient;
bool       isPcClientActive = false;
std::vector<WiFiClient> visualisationClients;

enum class Commands : uint8_t 
{
  StopSending = 0x08,
  StartSending
};

enum class Response : uint8_t
{
  Frame = 0x03,
  Ok    = 0x0F,
  Nok
};

enum class ClientId : uint8_t 
{
  Pc = 0x04,
  Visualisation = 0x05
};

void      configureSensor(Sensor*);
void      resetConnection();
void      sendResponse(WiFiClient*, Response);
uint8_t*  getSensorData();
uint16_t  getDistanceFromSensor(Sensor*);
uint8_t*  readBytesFromSocket(WiFiClient*, uint8_t);
bool      checkIfResponse(uint8_t* response);

void setup() 
{
  Serial.begin(9600);
  
  resetConnection();
    
  IPAddress localIp(192, 168, 1, 1);
  IPAddress gateway(192, 168, 1, 1);
  IPAddress subnet(255, 255, 255, 0);

  WiFi.softAPConfig(localIp, gateway, subnet);
  WiFi.softAP(ServerSsid, ServerPassword);

  server.begin();
  
  configureSensor(&frontSensor);
  configureSensor(&sideSensor);
}

void loop() 
{
  WiFiClient incomingClient = server.available();

  if (incomingClient) {
    while (incomingClient.available() < FrameSize) {
      Serial.print("Number of bytes in the socket: "); 
      Serial.println(incomingClient.available());
      Serial.println("Awaiting for client to respond...");
      delay(1000);
    }
    auto response = readBytesFromSocket(&incomingClient, FrameSize);

    if (! checkIfResponse(response)) {
      Serial.println("Incoming client's response is invalid.");
    }
    else {
      auto clientId = static_cast<ClientId>(response[FrameSize-1]);
      switch (clientId) 
      {
        case ClientId::Pc:
          Serial.println("PC device connected.");
          pcClient = incomingClient; 
          sendResponse(&pcClient, Response::Ok);
          break;
          
        case ClientId::Visualisation:
          Serial.println("Visualisation device connected.");
          visualisationClients.push_back(incomingClient);
          sendResponse(&visualisationClients.back(), Response::Ok);
          break;
          
        default:
          Serial.println("Unable to connect an unknown device.");
          Serial.print("ClientId: ");
          Serial.println(static_cast<int>(clientId));
          sendResponse(&incomingClient, Response::Nok);
          break;
      }
    }
  }

  auto currentSensorData = getSensorData();

  if (pcClient.connected()) {
    if (pcClient.available() >= FrameSize) {
      auto incomingMessage = readBytesFromSocket(&pcClient, FrameSize);
      
      if (checkIfResponse(incomingMessage)) {
        auto command = static_cast<Commands>(incomingMessage[FrameSize-1]);
        if (command == Commands::StartSending) {
          Serial.println("Command: StartSending");
          sendResponse(&pcClient, Response::Ok);
          isPcClientActive = true;
        }
        else if (command == Commands::StopSending) {
          Serial.println("Command: StopSending");
          sendResponse(&pcClient, Response::Ok);
          isPcClientActive = false;
        }
      }
    }
    if (isPcClientActive) {
      sendData(&pcClient, currentSensorData, FrameSize);
    }
  }
  for (auto& i : visualisationClients) {
    if (i.connected()) {
      sendData(&i, currentSensorData, FrameSize);
    }
  }
  delete currentSensorData;
  delay(DistanceMeasureIntervalInMillis);
}

void configureSensor(Sensor* sensor)
{
  pinMode(sensor->trigPin, OUTPUT);
  pinMode(sensor->echoPin, INPUT);
}

uint16_t getDistanceFromSensor(Sensor* sensor)
{
  digitalWrite(sensor->trigPin, LOW);
  delayMicroseconds(2);
  
  digitalWrite(sensor->trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(sensor->trigPin, LOW);
  
  uint16_t duration = pulseIn(sensor->echoPin, HIGH);
  uint16_t distance = duration * 0.017;
  
  Serial.print("Distance: ");
  Serial.println(distance);

  return distance;
}

void resetConnection()
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
}

uint8_t* getSensorData()
{
  //Serial.println("Sending distance...");
  
  uint16_t distanceToSendSide  = getDistanceFromSensor(&sideSensor);
  uint16_t distanceToSendFront = getDistanceFromSensor(&frontSensor);
  
  uint8_t* distanceBuffer = new uint8_t[FrameSize] 
  {
    static_cast<uint8_t>(Response::Frame),
    0x01,                     // FrameType::TwoSensor
    distanceToSendSide >> 8,
    distanceToSendSide,
    distanceToSendFront >> 8,
    distanceToSendFront
  };
  return distanceBuffer;
}

uint8_t* readBytesFromSocket(WiFiClient* socket, uint8_t FrameSize)
{
  Serial.println("Reading bytes from socket...");
  uint8_t i = 0;
  uint8_t* frame = new uint8_t[FrameSize] {0};
  byte currentByte = '\0';

  while (socket->available() < FrameSize)
    ;
  while (i < FrameSize) {
    frame[i++] = socket->read();
  }
  return frame;
}

void sendData(WiFiClient* socket, uint8_t* data, uint16_t size) 
{
  if (data) {
    socket->write((const uint8_t*) data, size);
  }
}

void sendResponse(WiFiClient* socket, Response response)
{
  uint8_t messageToSend[FrameSize] =
  {
    static_cast<uint8_t>(response),
    0xFF,
    0xFF,
    0xFF,
    0xFF,
    0xFF
  };
  socket->write((const uint8_t*) messageToSend, FrameSize);
}

bool checkIfResponse(uint8_t* response)
{
  if (response) {
    return response[0] == 0xFF && response[1] == 0xFF && response[2] == 0xFF && response[3] == 0xFF && response[4] == 0xFF; 
  }
  return false;
}
