#ifndef SIMULATIONCONTROLLER_H
#define SIMULATIONCONTROLLER_H

#include "sl_dominantqueue.h"
#include <QObject>
#include <QString>
#include <QVector3D>

class Backend;
class ObstaclesModel;

class SimulationController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector3D carPosition    READ carPosition        NOTIFY carPositionChanged)
    Q_PROPERTY(QVector3D carScale       READ carScale           NOTIFY carScaleChanged)
    Q_PROPERTY(QVector3D cameraPosition READ cameraPosition     NOTIFY cameraPositionChanged)

public:
    SimulationController(QObject* parent = nullptr);
    SimulationController(
        ObstaclesModel* obstaclesModel,
        float pixelRatio,
        QObject* parent = nullptr
    );

    void updateSensorsValues(int front, int side);

    QVector3D carPosition()     const;
    QVector3D carScale()        const;
    QVector3D cameraPosition()  const;

    void setCameraPosition     (const QVector3D&);
    void setCarPosition        (const QVector3D&);
    void setCarScale           (const QVector3D&);

signals:
    void cameraPositionChanged();
    void carPositionChanged();
    void carScaleChanged();

private:
    void setFrontDistanceValue(float value);
    void setCarVerticalPosition(float y);

    float m_frontDistanceInPx;
    float m_pixelRatio;

    QVector3D m_cameraPosition;
    QVector3D m_carPosition;
    QVector3D m_carScale;

    SimulationLib::DominantQueue m_frontSensorDominantQueue;
    ObstaclesModel* m_obstaclesModel;
};

#endif
