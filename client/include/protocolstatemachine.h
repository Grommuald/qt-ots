#ifndef PROTOCOLSTATEMANAGER_H
#define PROTOCOLSTATEMANAGER_H

#include "sl_connectionclient.h"
#include "sl_connectionstatus.h"
#include "sl_message.h"

#include <QObject>

class  SimulationController;

class ProtocolStateMachine : public QObject
{
    Q_OBJECT

public:
    explicit ProtocolStateMachine(
        SimulationController* simulationController,
        SimulationLib::ConnectionClient* sensorClient,
        QObject *parent = nullptr
    );
    explicit ProtocolStateMachine(QObject *parent = nullptr);
    ~ProtocolStateMachine();

signals:
    void frameReceived(SimulationLib::IncomingMessage* receivedFrame);
    void sensorResponse(SimulationLib::SensorResponse response);
    void sensorOk();
    void sensorNok();

private slots:
    void onReceiveFrame(SimulationLib::IncomingMessage* receivedFrame);
    void onIncomingSensorMessage(SimulationLib::IncomingMessage* incomingFrame);

private:
    SimulationController*  m_controller;
    SimulationLib::ConnectionClient* m_sensorClient;
    SimulationLib::IncomingMessage* m_currentlyReceivedFrame;
};

#endif // PROTOCOLSTATEMANAGER_H
