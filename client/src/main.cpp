#include "sl_connectionstatus.h"
#include "sl_debug.h"

#include "simulationcontroller.h"
#include "protocolstatemachine.h"
#include "visualisationclient.h"
#include "obstaclesmodel.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QScreen>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle("Material");

    qmlRegisterUncreatableType<SimulationLib::ConnectionStatus>(
        "qt.mock.server", 1, 0, "ConnectionStatus",
        "Cannot create ConnectionStatus in QML"
    );
    qmlRegisterType<Obstacle>();

    QQmlApplicationEngine engine;

    ObstaclesModel obstaclesModel;
    engine.rootContext()->setContextProperty("ObstaclesModel", &obstaclesModel);

    SimulationController simulationController(
        &obstaclesModel,
        QGuiApplication::primaryScreen()->devicePixelRatio()
    );
    DebugLog << "Pixel Ratio: " << QGuiApplication::primaryScreen()->devicePixelRatio();
    engine.rootContext()->setContextProperty("SimulationController", &simulationController);

    VisualisationClient sensor;
    engine.rootContext()->setContextProperty("Sensor", &sensor);

    ProtocolStateMachine psm(&simulationController, &sensor);
    engine.rootContext()->setContextProperty("ProtocolStateMachine", &psm);

    engine.load(QUrl(QStringLiteral("main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
