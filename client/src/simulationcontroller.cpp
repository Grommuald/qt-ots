#include "simulationcontroller.h"
#include "obstaclesmodel.h"
#include "sl_debug.h"

using namespace SimulationLib;

namespace
{
    const     quint32   DominantQueueLength           = 10;
    const     int       PixelCentimeterRatio          = 4;
    const     float     TopScreenEdgeVerticalPosition = 300.0f;
}

SimulationController::SimulationController(QObject* parent)
    : SimulationController(nullptr, 1, parent)
{

}

SimulationController::SimulationController(
    ObstaclesModel* obstaclesModel,
    float pixelRatio,
    QObject* parent
)
    : QObject(parent),
      m_pixelRatio(pixelRatio),
      m_obstaclesModel(obstaclesModel)
{
    m_obstaclesModel->setPixelRatio(m_pixelRatio);

    m_frontSensorDominantQueue.setQueueLength(DominantQueueLength);

    setCameraPosition( { 68.7f, 0.0f, 0.0f } );
    setCarPosition   ( { -297.0f, -12.0f, 1000.0f } );
    setCarScale      ( { 0.77f, 0.77f, 0.77f } );
}

void SimulationController::updateSensorsValues(int frontValueInCm, int sideValueInCm)
{
    auto currentFrontValueInCm = m_frontSensorDominantQueue.updateQueue(frontValueInCm).getCurrentDominantValue();

    setFrontDistanceValue(currentFrontValueInCm);
    m_obstaclesModel->updateObstacleInfo(currentFrontValueInCm, sideValueInCm);
}

void SimulationController::setFrontDistanceValue(float value)
{
    m_frontDistanceInPx = value * PixelCentimeterRatio;
    setCarVerticalPosition(TopScreenEdgeVerticalPosition - m_frontDistanceInPx);
}

QVector3D SimulationController::carPosition() const
{
    return m_carPosition;
}

QVector3D SimulationController::carScale() const
{
    return m_carScale;
}

QVector3D SimulationController::cameraPosition() const
{
    return m_cameraPosition;
}

void SimulationController::setCameraPosition(const QVector3D& cameraPosition)
{
    m_cameraPosition = cameraPosition;
    emit cameraPositionChanged();
}

void SimulationController::setCarPosition(const QVector3D& carPosition)
{
    m_carPosition = carPosition;
    emit carPositionChanged();
}

void SimulationController::setCarScale(const QVector3D& carScale)
{
    m_carScale = carScale;
    emit carScaleChanged();
}

void SimulationController::setCarVerticalPosition(float y)
{
    m_carPosition.setY(y);
    emit carPositionChanged();
}


