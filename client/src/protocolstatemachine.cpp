#include "protocolstatemachine.h"
#include "simulationcontroller.h"

#include "sl_protocolcommands.h"
#include "sl_debug.h"

using namespace SimulationLib;

ProtocolStateMachine::ProtocolStateMachine(
        SimulationController* simulationController,
        ConnectionClient* sensorClient,
        QObject *parent)
    :
    QObject                     (parent),
    m_controller                (simulationController),
    m_sensorClient              (sensorClient)
{
    connect(m_sensorClient, &ConnectionClient::sensorMessage,
            this, &ProtocolStateMachine::onIncomingSensorMessage);
    connect(this, &ProtocolStateMachine::frameReceived,
            this, &ProtocolStateMachine::onReceiveFrame);
}

ProtocolStateMachine::ProtocolStateMachine(QObject *parent)
    : ProtocolStateMachine(nullptr, nullptr, parent)
{

}

ProtocolStateMachine::~ProtocolStateMachine()
{

}

void ProtocolStateMachine::onReceiveFrame(IncomingMessage* receivedFrame)
{
    auto parsedMessage = MessageParser::parse(receivedFrame);
    delete receivedFrame;

    m_controller->updateSensorsValues(
        parsedMessage->argument[2].convert(),
        parsedMessage->argument[1].convert()
    );
}

void ProtocolStateMachine::onIncomingSensorMessage(IncomingMessage *incomingMessage)
{
    auto messageType = MessageParser::type(incomingMessage);

    switch (messageType)
    {
    case SensorResponse::Ok:
        emit sensorResponse(SensorResponse::Ok);
        emit sensorOk();
        break;

    case SensorResponse::Nok:
        emit sensorResponse(SensorResponse::Nok);
        emit sensorNok();
        break;

    case SensorResponse::Frame:
        emit frameReceived(incomingMessage);
        break;
    }
}
