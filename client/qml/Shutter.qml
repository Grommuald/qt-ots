import QtQuick 2.0

Item {
    property bool leftToRight: true
    property bool buttonEnabled: true
    property bool isShutterButtonPullable: true
    property string shutterButtonState
    property string shutterButtonText
    property string shutterButtonIconImageSource
    property string shutterState
    property int windowWidth
    property int verticalDrawerButtonPosition: height * 0.5 - 25

    id: shutter
    width: 400
    height: parent.height
    z: 5

    onShutterStateChanged: {
        state = shutterState;
    }

    Rectangle {
        id: shutterBackground
        anchors { fill: parent }
        color: "#303030"
    }

    Loader {
        id: buttonLoader
        sourceComponent: leftToRight ? leftsideButton : rightsideButton;
        z: -1
        states: [
            State {
                name: "leftToRight"
                when: leftToRight
                PropertyChanges {
                    target: buttonLoader
                    y: verticalDrawerButtonPosition
                    anchors { left: shutter.right }
                }
                PropertyChanges {
                    target: shutter; state: "idleRight"
                }
            },
            State {
                name: "rightToLeft"
                when: !leftToRight
                PropertyChanges {
                    target: buttonLoader
                    y: verticalDrawerButtonPosition
                    anchors { right: shutter.left }
                }
                PropertyChanges {
                    target: shutter; state: "idleRight"
                }
            }
        ]
    }

    Component {
        id: leftsideButton

        LeftsideDrawerButton {
            enabled: buttonEnabled
            state: shutterButtonState
            isPullable: isShutterButtonPullable

            Text {
                font.pixelSize: 10
                color: "white"
                anchors { left: parent.left; leftMargin: 2; verticalCenter: parent.verticalCenter }
                text: shutterButtonText
            }

            Image {
                anchors { right: parent.right; rightMargin: 1; verticalCenter: parent.verticalCenter }
                width: 12
                height: 12
                fillMode: Image.PreserveAspectFit
                smooth: true
                source: shutterButtonIconImageSource
            }

            MouseArea {
                id: dragArea

                enabled: shutterState  === 'idleLeft' || shutterState  === 'idleRight'
                anchors.fill: parent

                onPressed: {
                    if (shutterState  === 'idleLeft') {
                        shutterState  = 'movingRight';
                    }
                    else if (shutterState  === 'idleRight') {
                        shutterState  = 'movingLeft';
                    }
                }
            }
        }
    }

    Component {
        id: rightsideButton

        RightsideDrawerButton {
            enabled: buttonEnabled
            state: shutterButtonState
            isPullable: isShutterButtonPullable
            isLoose: false

            Text {
                font.pixelSize: 10
                color: "white"
                anchors { right: parent.right; rightMargin: 2; verticalCenter: parent.verticalCenter }
                text: shutterButtonText
            }

            Image {
                anchors { left: parent.left; leftMargin: 1; verticalCenter: parent.verticalCenter }
                width: 12
                height: 12
                fillMode: Image.PreserveAspectFit
                smooth: true
                source: shutterButtonIconImageSource
            }

            MouseArea {
                id: dragArea

                enabled: shutterState  === 'idleLeft' || shutterState  === 'idleRight'
                anchors.fill: parent

                onPressed: {
                    if (shutterState  === 'idleLeft') {
                        shutterState  = 'movingRight';
                    }
                    else if (shutterState  === 'idleRight') {
                        shutterState  = 'movingLeft';
                    }
                }
            }
        }
    }

    // Might also think about a seperate case for z parameter for rightToLeft shutter
    // for now it only supports leftToRight shutter since when I put there a conditional
    // it flickers in a weird way.
    states: [
        State {
            name: "idleRight"
            PropertyChanges {
                target: shutter; z: 6; x: leftToRight ? 0 : windowWidth; shutterState: "idleRight"
            }
        },
        State {
            name: "movingRight"
            PropertyChanges {
                target: shutter; z: 6; x: leftToRight ? 0 : windowWidth; shutterState: "movingRight"
            }
        },
        State {
            name: "movingLeft"
            PropertyChanges {
                target: shutter; z: 6; x: leftToRight ? -shutter.width : windowWidth - shutter.width; shutterState: "movingLeft"
            }
        },
        State {
            name: "idleLeft"
            PropertyChanges {
                target: shutter; z: 5; x: leftToRight ? -shutter.width : windowWidth - shutter.width; shutterState: "idleLeft"
            }
        }
    ]

    transitions: [
        Transition {
            to: "movingRight";
            NumberAnimation { properties: "x"; easing.type: Easing.InOutQuad; duration: 1000 }
            onRunningChanged: {
                if ((shutterState  === "movingRight") && (!running)) {
                    isShutterButtonPullable = !leftToRight;
                    shutterState = "idleRight";
                }
            }
        },
        Transition {
            to: "movingLeft";
            NumberAnimation { properties: "x"; easing.type: Easing.InOutQuad; duration: 1000 }
            onRunningChanged: {
                if ((shutterState  === "movingLeft") && (!running)) {
                    isShutterButtonPullable = leftToRight;
                    shutterState  = "idleLeft";
                }
            }
        }
    ]
}
