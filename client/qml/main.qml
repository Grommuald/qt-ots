import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls.Styles 1.4
import QtStudio3D 2.1
import qt.mock.server 1.0

ApplicationWindow {
    property int windowWidth: 900
    property int windowHeight: 600

    id: clientApplicationWindow
    visible: true
    title: qsTr("Visualisation")

    Material.theme: Material.Dark
    Material.accent: Material.Orange

    // Unable window-resizing
    minimumWidth: windowWidth
    minimumHeight: windowHeight

    maximumWidth: minimumWidth
    maximumHeight: minimumHeight

    Connections {
        target: Sensor

        onStatusChanged: {
            refreshSensorStatus();
        }
    }

    function refreshSensorStatus() {
        simulation.visible = Sensor.status === ConnectionStatus.Connected;
        unavailabilityLabel.visible = !simulation.visible;

        sensorConnectionPortTextField.enabled = Sensor.status !== ConnectionStatus.Connected;
        sensorConnectionAddressTextField.enabled = Sensor.status !== ConnectionStatus.Connected;

        connectionShutter.buttonEnabled = simulation.visible;

        if (Sensor.status === ConnectionStatus.Connected) {
            sensorConnectionButton.text = qsTr("Disconnect");
            sensorConnectionIndicator.state = "connected";
            connectionShutter.state = "movingRight";
        }
        else if (Sensor.status === ConnectionStatus.Disconnected
                || Sensor.status === ConnectionStatus.Failure)
        {
            sensorConnectionButton.text = qsTr("Connect");

            if (Sensor.status === ConnectionStatus.Disconnected) {
                sensorConnectionIndicator.state = "disconnected";
            }
            else if (Sensor.status === ConnectionStatus.Failure) {
                connectionShutter.state = "movingLeft";
                sensorConnectionIndicator.state = "error";
            }
        }
    }


    Component.onCompleted: {
        refreshSensorStatus();
        connectionShutter.state = "idleLeft";
    }

    Shutter {
        id: connectionShutter
        leftToRight: false
        shutterButtonText: qsTr("Connection")
        shutterButtonIconImageSource: "media/images/connection_icon.png"
        isShutterButtonPullable: simulation.visible
        windowWidth: clientApplicationWindow.width
        width: clientApplicationWindow.width * 0.35

        GroupBox {
            id: connectionGroupBox
            title: qsTr("Connection")
            anchors { fill: parent; topMargin: 10; leftMargin: 10; rightMargin: 10; bottomMargin: 10 }

            Item {
                width: parent.width
                anchors { top: parent.top; bottom: parent.bottom }

                GroupBox {
                    id: sensorConnectionGroupBox
                    anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: 10 }
                    height: 80
                    font { pixelSize: 12 }
                    title: qsTr("Sensors")

                    Item {
                        id: sensorConnectionWidgetHolder
                        height: 40
                        anchors { top: parent.top; left: parent.left; right: parent.right }

                        ConnectionIndicator {
                            id: sensorConnectionIndicator
                            anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
                        }

                        TextField {
                            id: sensorConnectionPortTextField
                            anchors { left: sensorConnectionIndicator.right; leftMargin: 10 }
                            width: 50
                            horizontalAlignment: TextEdit.AlignHCenter
                            selectByMouse: true
                            placeholderText: qsTr("Port")
                            text: '5000'
                            inputMethodHints: Qt.ImhPreferNumbers
                            validator: RegExpValidator { regExp: /[0-9]+/ }
                            maximumLength: 4

                            onTextChanged: Sensor.port = text

                            Component.onCompleted: {
                                Sensor.port = text;
                            }
                        }

                        TextField {
                            id: sensorConnectionAddressTextField
                            anchors { left: sensorConnectionPortTextField.right; leftMargin: 10 }

                            width: 80

                            selectByMouse: true
                            placeholderText: qsTr("@Address")
                            text: '192.168.1.1'

                            onTextChanged: Sensor.address = text

                            Component.onCompleted: {
                                Sensor.address = text;
                            }
                        }

                        Button {
                            id: sensorConnectionButton
                            font { pixelSize: 12 }
                            anchors { left: sensorConnectionAddressTextField.right; right: parent.right; leftMargin: 10 }
                            height: 40
                            onClicked: {
                                Sensor.setConnection();
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: connectionShutterShadow
        z: 1
        anchors { top: parent.top; bottom: parent.bottom; left: parent.left; right: parent.right }
        color: "black"
        opacity: (Math.abs(clientApplicationWindow.width - connectionShutter.x) / (clientApplicationWindow.width - connectionShutter.width))
    }

    Column {
        id: unavailabilityLabel
        anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: clientApplicationWindow.width * 0.23 }
        opacity: 0.5

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Connect the sensors")
            font.pixelSize: 16
        }
        Label {
            text: qsTr("to show the visualisation.")
            font.pixelSize: 16
        }
    }


    Item {
        id: simulation
        anchors { fill: parent }

        Label {
            id: nearestObstacleDistanceLabel
            z: 2
            anchors { horizontalCenter: nearestObstacleDistanceLine.horizontalCenter; bottom: nearestObstacleDistanceLine.top; bottomMargin: 8 }
            font.pixelSize: 14
            text: ObstaclesModel.nearestObstacleDistanceInCm + " cm"
            color: "#ffffff"
        }

        Rectangle {
            id: nearestObstacleDistanceLine
            z: 2
            anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: ObstaclesModel.nearestObstacleDistanceInPx }
            width: 2
            height: 0.75 * parent.height;
            color: ObstaclesModel.distanceMarkerColor
        }

        Repeater {
            id: obstaclesRepeater
            z: 1
            model: ObstaclesModel
            delegate: Rectangle {
                z: 1
                anchors { left: parent.left; leftMargin: distanceFromSensor; top: parent.top; topMargin: verticalPosition }
                width: ObstaclesModel.obstacleWidth
                height: ObstaclesModel.obstacleHeight
                color: obstacleColor
                radius: 4

                Component.onCompleted:  {
                    console.log("y: " + verticalPosition + "  x: " + distanceFromSensor);
                }
            }
        }

        Studio3D {
            id: presentation
            anchors { fill: parent }

            Presentation {
                source: "media/car_and_obstacle/car_and_obstacle.uia"

                DataInput {
                    name: "carPositionDataInput"
                    value: SimulationController.carPosition
                }

                DataInput {
                    name: "cameraPositionDataInput"
                    value: SimulationController.cameraPosition
                }

                DataInput {
                    name: "carScaleDataInput"
                    value: SimulationController.carScale
                }
            }
        }
    }
}
