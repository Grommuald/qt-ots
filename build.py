import os, sys, re, glob, hashlib, subprocess, getopt
from termcolor import colored

def listAllFilesWithExtension(dirPath, extension):
    fileList = []

    os.chdir(dirPath)
    for file in glob.glob("*." + extension):
        fileList.append(file)
    
    os.chdir('../../')
    return fileList

def isDirContentChanged(dirPath):
    m = hashlib.md5()
    listOfFiles = [file for file in os.listdir(dirPath) if os.path.isfile(os.path.join(dirPath, file))]
    m.update(' '.join(listOfFiles).encode('utf-8'))
    
    currentStateHash = m.hexdigest()
    previousStateHash = ''
    
    fileChangesName = '.file_changes'
    fileChangesPath = dirPath + '/' + fileChangesName

    # We don't want our hash file on the list
    if fileChangesName in listOfFiles:
        listOfFiles.remove(fileChangesName)

    if os.path.exists(fileChangesPath):
        with open(fileChangesPath, 'r') as f:
            previousStateHash = f.read()
        return currentStateHash != previousStateHash
    else:
        with open(fileChangesPath, 'w+') as f:
            f.write(currentStateHash)
        return True

def getContentBeforeAndAfterTag(beginTag, endTag, filePath):
    partBeforeTag = ''
    partAfterTag = ''
    
    with open(filePath, "r") as f:
        reachedBeginningTag = False
        reachedEndingTag = False

        for line in f:
            if not reachedBeginningTag:
                if re.search(beginTag, line):
                    reachedBeginningTag = True
                else:
                    partBeforeTag += line
            else:
                if not reachedEndingTag:
                    if re.search(endTag, line):
                        reachedEndingTag = True
                else:
                    partAfterTag += line

    return partBeforeTag, partAfterTag

def refreshWrapsInCMakeLists(projName): 
    VariableName = projName.upper() + '_HEADER_DIR'
    BeginningTag = '# HEADER_WRAPS_BEGIN'
    EndingTag = '# HEADER_WRAPS_END'
    CMakePath = projName + '/src/CMakeLists.txt'

    partBeforeWrapSection, partAfterWrapSection = getContentBeforeAndAfterTag(BeginningTag, EndingTag, CMakePath)

    listOfHeaders = listAllFilesWithExtension(projName + '/include', 'h')

    wraps = {}
    for header in listOfHeaders:
        key = os.path.splitext(header)[0] + '_SRC'
        wraps[key] = 'qt5_wrap_cpp(' + key + ' ${' + VariableName + '}/' + header + ')'
    
    with open(CMakePath, 'w') as f:
        f.write(partBeforeWrapSection)
        f.write(BeginningTag + '\n')
        f.write('\n'.join(list(wraps.values())))
        f.write('\n' + EndingTag + '\n')
        f.write(partAfterWrapSection)
    
    return list(wraps.keys())

def refreshSourcesInCMakeLists(projName):
    VariableName = projName.upper() + '_PROJECT_NAME'
    BeginningTag = '# SOURCES_BEGIN'
    EndingTag = '# SOURCES_END'
    CMakePath = projName + '/src/CMakeLists.txt'

    partBeforeSourceSection, partAfterSourceSection = getContentBeforeAndAfterTag(BeginningTag, EndingTag, CMakePath)

    listOfSources = listAllFilesWithExtension(projName + '/src', 'cpp')
    
    serverSourcesSection = []
    for source in listOfSources:
        serverSourcesSection.append(source)
    
    with open(CMakePath, 'w') as f:
        f.write(partBeforeSourceSection)
        f.write(BeginningTag + '\n')
        f.write('set(\n')
        f.write(projName + 'Sources\n')
        f.write('\n'.join(serverSourcesSection))
        f.write('\n)\n' + EndingTag + '\n')
        f.write(partAfterSourceSection)

def refreshTargetInCMakeLists(projName, isStandAlone=True, wrapSourcesNames=''): 
    VariableName = projName.upper() + '_PROJECT_NAME'
    BeginningTag = '# TARGET_BEGIN'
    EndingTag = '# TARGET_END'
    CMakePath = projName + '/src/CMakeLists.txt'

    partBeforeTargetSection, partAfterTargetSection = getContentBeforeAndAfterTag(BeginningTag, EndingTag, CMakePath)
 
    with open(CMakePath, 'w') as f:
        f.write(partBeforeTargetSection)
        f.write(BeginningTag + '\n')
        commandName = 'add_executable' if isStandAlone else 'add_library' 
        
        f.write(commandName + '(\n${' + VariableName + '}')
        f.write('\n${' + projName + 'Sources}')
        for wrapping in wrapSourcesNames:
            f.write('\n${' + wrapping + '}')
        
        if isStandAlone:
            f.write('\n${QT_RESOURCES}')
        f.write('\n)\n')
        
        f.write(EndingTag + '\n')
        f.write(partAfterTargetSection)

def checkForFiles(projName):
    filePaths = ['CMakeLists.txt', 'src/CMakeLists.txt']

    failure = False

    for file in filePaths:
        currentPath = projName + '/' + file
        if os.path.exists(currentPath):
            print(colored(f'--- {currentPath:32} --- OK', 'green'))
        else:
            print(colored(f'--- {currentPath:32} --- NOK', 'red'))
            failure = True

    if failure:
        print(colored('--- FATAL ERROR: some of CMakeLists.txt files are missing', 'red'))
        sys.exit(2)

def createRunScript():
    runScriptPath = 'run.py'
    with open(runScriptPath, 'w') as script:
        script.write(\
        "import subprocess, os\n\n" +\
        "os.chdir('build/server/src')\n" +\
        "subprocess.Popen(['./server'])\n" +\
        "os.chdir('../../client/src')\n" +\
        "subprocess.Popen(['./client'])")

# Updates CMakeLists.txt inside the /src directories for every project (if needed)
def main():
    projectNames = {'client'        :   'stand-alone', 
                    'server'        :   'stand-alone',
                    'common_lib'    :   'lib'} 
    try:
        opts, args = getopt.getopt(sys.argv[1:], "erp", ["run", "rebuild", "purge"])
    except getopt.GetoptError as err:
        print(colored(err, 'red'))
        sys.exit(2)
    
    rebuild = False
    purge = False
    runAfterSuccessfulBuild = False

    for o, a in opts:
        if o in ('-r', '--rebuild'):
            rebuild = True
        elif o in ('-p', '--purge'):
            purge = True
        elif o in ('-e', '--run'):
            runAfterSuccessfulBuild = True

    if rebuild and purge:
        print(colored('--- Cannot use --purge with --rebuild', 'red'))
        sys.exit(2)

    if rebuild or purge:
        print(colored('--- Removing build directory...', 'green'))
        subprocess.call(['rm', '-rf', 'build'])
    
    if purge:
        print(colored('--- END', 'green'))
        sys.exit(0)

    if(rebuild or not os.path.isdir('build')):
        print(colored('--- Creating build directory...', 'green'))
        subprocess.call(['mkdir', 'build'])
    
    print(colored('--- Checking for files...', 'blue'))
    for projectName in projectNames.keys():
        checkForFiles(projectName)

    print(colored('--- Checking for changes inside projects directories...', 'blue'))
    
    for projectName, projectType in projectNames.items():
        wrapSourcesNames = []
        isStandAlone = projectType == 'stand-alone'
        
        if isDirContentChanged(projectName + '/include'):
            print(colored('--- Headers inside ' + projectName + ' require new wrappings...', 'blue'))
            wrapSourcesNames = refreshWrapsInCMakeLists(projectName)

        if isDirContentChanged(projectName + '/src'):
            print(colored('--- Sources inside ' + projectName + ' require refreshment...' , 'blue'))

            refreshSourcesInCMakeLists(projectName)
            refreshTargetInCMakeLists(projectName, isStandAlone, wrapSourcesNames)

    # Run the actual CMake
    print(colored('--- Running CMake build system...', 'blue'))
    os.chdir('build')
    subprocess.call(['cmake', '-G', '..'])
    # Then run Makefiles
    makeExitCode = subprocess.call(['make'])
    os.chdir('../')

    if makeExitCode == 0 and not os.path.exists('run.py'):
        createRunScript()

    if runAfterSuccessfulBuild:
        subprocess.call(['python3', 'run.py'])

if __name__ == "__main__":
    main()
