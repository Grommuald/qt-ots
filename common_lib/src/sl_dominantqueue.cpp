#include "sl_dominantqueue.h"
#include <QMap>

namespace SimulationLib
{
DominantQueue::DominantQueue()
    : m_queueLength(10)
{

}

DominantQueue::DominantQueue(int maxNumberOfElements)
    : m_queueLength(maxNumberOfElements)
{

}

DominantQueue::~DominantQueue()
{

}

void DominantQueue::setQueueLength(int length)
{
    m_queueLength = length;
}

DominantQueue& DominantQueue::updateQueue(int newValue)
{
    m_latestValues.enqueue(newValue);
    if (m_latestValues.count() > m_queueLength) {
        m_latestValues.dequeue();
    }

    QMap<int, QPair<quint8, int>> currentValues;
    for (const auto& i : m_latestValues) {
        currentValues[i].first++;       // count of every occurence of i-value in the queue
        currentValues[i].second = i;
    }

    QPair<quint8, int> maxValue = { 0, -1 };
    for (auto& i : currentValues.values()) {
        if (i.second > maxValue.second) {
            maxValue = i;
        }
    }
    m_currentDominantValue = maxValue.second;

    return *this;
}

int DominantQueue::getCurrentDominantValue() const
{
    return m_currentDominantValue;
}

int DominantQueue::getCurrentLength() const
{
    return m_latestValues.count();
}
}
