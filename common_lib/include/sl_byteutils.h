#ifndef BYTEUTILS_H
#define BYTEUTILS_H

#include <QtGlobal>
#include <QByteArray>

namespace SimulationLib
{
class ByteUtils
{
public:
    static quint16 toUInt16(const QByteArray& array);
    static qint16  toSInt16(const QByteArray& array);
    static quint8  toUInt8 (const QByteArray& array);
    static qint8   toSInt8 (const QByteArray& array);
};
}
#endif // BYTEUTILS_H
