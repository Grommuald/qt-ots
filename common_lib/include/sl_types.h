#ifndef TYPES_H
#define TYPES_H

#include <QByteArray>

namespace SimulationLib
{
enum class ValueType : quint8
{
    UInt16,
    SInt16,
    UInt8,
    SInt8
};

enum class EntryType : quint8
{
    Frame
};

enum class FrameType : quint8
{
    OneSensor,
    TwoSensor,
    FourSensor,
    EightSensor
};

enum class FieldType : quint8
{
    DistanceField,
    VelocityField,
    TimeField,
    AngleField,
    BooleanField
};

enum class Unit : quint8
{
    Centimeter,
    Milliseconds,
    KilometersPerHour,
    Degree,
    Bit
};

struct Output
{
    ValueType valueType;
    FieldType fieldType;
    Unit unit;
};
}
#endif // TYPES_H
