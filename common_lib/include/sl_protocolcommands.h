#ifndef PROTOCOLCOMMANDS_H
#define PROTOCOLCOMMANDS_H

#include <QtGlobal>
#include <QByteArray>

namespace SimulationLib
{
enum class SensorResponse : quint8
{
    Frame = 0x03,
    Ok    = 0x0F,
    Nok,
};

enum class ClientResponse : quint8
{
    SayHiAsReader        = 0x04,
    SayHiAsClient        = 0x05,
    StopSendingToPc      = 0x08,
    StartSendingToPc,
};
}
#endif // PROTOCOLCOMMANDS_H
