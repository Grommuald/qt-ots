#ifndef ARGUMENT_H
#define ARGUMENT_H

#include "sl_types.h"

namespace SimulationLib
{
struct Argument
{
    ValueType type;
    union
    {
        quint16 uint16_val;
        qint16  sint16_val;
        quint8  uint8_val;
        qint8   sint8_val;
    }
    value;

    int convert()      const;
    QByteArray fetch() const;

    operator quint16() const;
    operator qint16()  const;
    operator quint8()  const;
    operator qint8()   const;
};

class ArgumentFactory
{
public:
    static Argument createUInt16(const quint16& = 0);
    static Argument createSInt16(const qint16& = 0);
    static Argument createUInt8 (const quint8& = 0);
    static Argument createSInt8 (const qint8& = 0);
};
}
#endif // ARGUMENT_H
