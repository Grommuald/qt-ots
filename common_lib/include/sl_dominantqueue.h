#ifndef DOMINANTQUEUE_H
#define DOMINANTQUEUE_H

#include <QQueue>

namespace SimulationLib
{
class DominantQueue
{
public:
    DominantQueue();
    DominantQueue(int queueLength);
    ~DominantQueue();

    void setQueueLength(int length);
    DominantQueue& updateQueue(int newValue);
    int getCurrentDominantValue() const;
    int getCurrentLength() const;

private:
    QQueue<int> m_latestValues;
    int m_currentDominantValue;
    int m_queueLength;
};
}

#endif
