import QtQuick 2.0

Rectangle {
    id: connectionIndicator
    radius: 2.0
    width: 10
    color: "#ff3030"
    opacity: 0.5

    states: [
        State {
            name: "disconnected"
            PropertyChanges {
                target: connectionIndicator
                color: "#ff3030"
            }
        },
        State {
            name: "error"
            PropertyChanges {
                target: connectionIndicator
                color: "#ff3030"
            }
        },
        State {
            name: "connected"
            PropertyChanges {
                target: connectionIndicator
                color: "#30ff41"
            }
        }
    ]

    transitions: [
        Transition {
            to: "disconnected"
            ColorAnimation {
                to: "#ff3030"; duration: 3000
            }
        },
        Transition {
            to: "error"
            SequentialAnimation {
                loops: 3
                ColorAnimation {
                    to: "#fffb30"; duration: 1000
                }
                PauseAnimation {
                    duration: 1000
                }
                ColorAnimation {
                    to: "#ff3030"; duration: 1000
                }
            }
        },
        Transition {
            to: "connected"
            ColorAnimation {
                to: "#30ff41"; duration: 3000
            }
        }
    ]
}
