#include "simulationcontroller.h"
#include "obstaclesmodel.h"
#include "../common_lib/include/sl_debug.h"

using namespace SimulationLib;

namespace
{
    const     quint32   DominantQueueLength           = 10;
    const     int       PixelCentimeterRatio          = 8;
    const     float     TopScreenEdgeVerticalPosition = 540.0f;
}

SimulationController::SimulationController(QObject* parent)
    : SimulationController(nullptr, 1, parent)
{

}

SimulationController::SimulationController(ObstaclesModel* obstaclesModel, float pixelRatio, QObject* parent)
    : QObject(parent),
      m_pixelRatio(pixelRatio),
      m_obstaclesModel(obstaclesModel)
{
    m_obstaclesModel->setPixelRatio(m_pixelRatio);

    m_frontSensorDominantQueue.setQueueLength(DominantQueueLength);

    setCameraPosition( { 68.7f, 0.0f, 0.0f } );
    setCarPosition   ( { -857.0f, -11.98f, 66.1f } );
    setCarScale      ( { 1.45f, 1.45f, 1.45f } );
}

void SimulationController::updateSensorsValues(int frontValueInCm, int sideValueInCm)
{
    auto currentFrontValueInCm = m_frontSensorDominantQueue.updateQueue(frontValueInCm).getCurrentDominantValue();

    setFrontDistanceValue(currentFrontValueInCm);
    m_obstaclesModel->updateObstacleInfo(currentFrontValueInCm, sideValueInCm);
}

void SimulationController::setFrontDistanceValue(float value)
{
    m_frontDistanceInPx = value * PixelCentimeterRatio;
    setCarVerticalPosition(TopScreenEdgeVerticalPosition - m_frontDistanceInPx);
}

QVector3D SimulationController::carPosition() const
{
    return m_carPosition;
}

QVector3D SimulationController::carScale() const
{
    return m_carScale;
}

QVector3D SimulationController::cameraPosition() const
{
    return m_cameraPosition;
}

void SimulationController::setCameraPosition(const QVector3D& cameraPosition)
{
    m_cameraPosition = cameraPosition;
    emit cameraPositionChanged();
}

void SimulationController::setCarPosition(const QVector3D& carPosition)
{
    m_carPosition = carPosition;
    emit carPositionChanged();
}

void SimulationController::setCarScale(const QVector3D& carScale)
{
    m_carScale = carScale;
    emit carScaleChanged();
}

void SimulationController::setCarVerticalPosition(float y)
{
    m_carPosition.setY(y);
    emit carPositionChanged();
}


