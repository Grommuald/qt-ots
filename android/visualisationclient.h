#ifndef VISUALISATIONCLIENT_H
#define VISUALISATIONCLIENT_H

#include "..\common_lib\include\sl_connectionclient.h"

class VisualisationClient : public SimulationLib::ConnectionClient
{
    Q_OBJECT

public:
    explicit VisualisationClient(QObject* parent = nullptr);
    void onConnectedToServer() final;
};

#endif
