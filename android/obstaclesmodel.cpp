#include "obstaclesmodel.h"
#include "../common_lib/include/sl_debug.h"
using namespace SimulationLib;

namespace
{
    const quint8 ObstaclePixelCentimeterRatio = 8;
    const quint8 DistanceBetweenFrontSensorAndSideSensorInPx = ObstaclePixelCentimeterRatio * 8;
    const int DistanceEpsilonInPx = ObstaclePixelCentimeterRatio * 3;
    const int LeftEdgeSideSensorDistance = 246;
    const int ShortDistanceInCm = 50;
    const int MediumDistanceInCm = 100;
    const quint8 ObstacleDominantDistanceQueueLength = 20;
}

Obstacle::Obstacle(QObject *parent)
    : QObject (parent)
{
    m_dominantDistanceQueue.setQueueLength(ObstacleDominantDistanceQueueLength);
}

void Obstacle::setDistance(int distanceInCm, float pixelRatio)
{
    m_dominantDistanceQueue.updateQueue(distanceInCm);

    auto dominantValue = m_dominantDistanceQueue.getCurrentDominantValue();
    m_distanceFromSensorInCm = dominantValue;
    m_distanceFromSensorInPx = (LeftEdgeSideSensorDistance +
            dominantValue * ObstaclePixelCentimeterRatio) / pixelRatio;

    if (dominantValue < ShortDistanceInCm) {
        m_color = "red";
    }
    else if (dominantValue < MediumDistanceInCm) {
        m_color = "yellow";
    }
    else if (dominantValue >= MediumDistanceInCm) {
        m_color = "green";
    }
}

void Obstacle::setVerticalPosition(float newVerticalPositionInPx)
{
    m_verticalPositionInPx = newVerticalPositionInPx;
}

float Obstacle::distanceFromSensorInPx() const
{
    return m_distanceFromSensorInPx;
}

int Obstacle::distanceFromSensorInCm() const
{
    return m_distanceFromSensorInCm;
}

float Obstacle::verticalPositionInPx() const
{
    return m_verticalPositionInPx;
}

QString Obstacle::color() const
{
    return m_color;
}

ObstaclesModel::ObstaclesModel(QObject *parent)
    : QAbstractListModel(parent),
      m_pixelRatio(1.0f),
      m_nearestObstacleDistanceInPx(1000),
      m_nearestObstacleDistanceInCm(1000)
{

}

ObstaclesModel::~ObstaclesModel()
{
    for (auto i : m_obstaclesMap.values()) {
        delete i;
    }
}

float ObstaclesModel::nearestObstacleDistanceInPx() const
{
    return m_nearestObstacleDistanceInPx;
}

int ObstaclesModel::nearestObstacleDistanceInCm() const
{
    return m_nearestObstacleDistanceInCm;
}

QString ObstaclesModel::distanceMarkerColor() const
{
    return m_distanceMarkerColor;
}

void ObstaclesModel::setPixelRatio(float pixelRatio)
{
    m_pixelRatio = pixelRatio;
    setObstacleWidth(ObstaclePixelCentimeterRatio);
    setObstacleHeight(ObstaclePixelCentimeterRatio);
}

void ObstaclesModel::setObstacleWidth(float width)
{
    m_obstacleWidth = width;
    emit obstacleWidthChanged();
}

void ObstaclesModel::setObstacleHeight(float height)
{
    m_obstacleHeight = height;
    emit obstacleHeightChanged();
}

void ObstaclesModel::findNearestObstacleDistance()
{
    int currentNearestObstacleDistanceInCm = 0;
    float currentNearestObstacleDistanceInPx = .0f;
    bool firstValueSet = false;

    for (const auto& i : m_obstaclesMap) {
        auto currentDistanceInCm = i->distanceFromSensorInCm();
        auto currentDistanceInPx = i->distanceFromSensorInPx();

        if (!firstValueSet) {
            currentNearestObstacleDistanceInCm = currentDistanceInCm;
            currentNearestObstacleDistanceInPx = currentDistanceInPx;
            firstValueSet = true;
        }
        else {
            if (currentDistanceInCm < currentNearestObstacleDistanceInCm) {
                currentNearestObstacleDistanceInCm = currentDistanceInCm;
                currentNearestObstacleDistanceInPx = currentDistanceInPx;
            }
        }
    }

    if (currentNearestObstacleDistanceInCm != m_nearestObstacleDistanceInCm) {
        m_nearestObstacleDistanceInPx = currentNearestObstacleDistanceInPx;
        m_nearestObstacleDistanceInCm = currentNearestObstacleDistanceInCm;

        if (m_nearestObstacleDistanceInCm < ShortDistanceInCm) {
            m_distanceMarkerColor = "red";
        }
        else if (m_nearestObstacleDistanceInCm < MediumDistanceInCm) {
            m_distanceMarkerColor = "yellow";
        }
        else {
            m_distanceMarkerColor = "green";
        }
        emit nearestObstacleDistanceChanged();
    }
}

void ObstaclesModel::removePeaks(int frontSensorValueInCm)
{
    if (m_obstaclesMap.contains(frontSensorValueInCm-1) && m_obstaclesMap.contains(frontSensorValueInCm+1)) {
        auto previous = m_obstaclesMap[frontSensorValueInCm-1]->distanceFromSensorInCm();
        auto current  = m_obstaclesMap[frontSensorValueInCm]  ->distanceFromSensorInCm();
        auto next     = m_obstaclesMap[frontSensorValueInCm+1]->distanceFromSensorInCm();

        auto averageValue = (previous + next) / 2;

        if (current > averageValue + DistanceEpsilonInPx
         || current < averageValue - DistanceEpsilonInPx)
        {
            m_obstaclesMap[frontSensorValueInCm]->setDistance(averageValue, m_pixelRatio);
        }
    }
}

void ObstaclesModel::updateModel()
{
    auto start_index = createIndex(0, 0);
    auto end_index = createIndex(m_obstaclesMap.count() - 1, 0);

    emit dataChanged(start_index, end_index);
}

void ObstaclesModel::updateObstacleInfo(int frontSensorValueInCm, int sideSensorValueInCm)
{
    m_currentFrontSensorValue = frontSensorValueInCm;

    if (!m_obstaclesMap.contains(frontSensorValueInCm)) {
        m_currentlyAddedObstacle = new Obstacle();
        m_currentlyAddedObstacle
                ->setVerticalPosition((frontSensorValueInCm * ObstaclePixelCentimeterRatio + DistanceBetweenFrontSensorAndSideSensorInPx) / m_pixelRatio);

        insertRows(m_obstaclesMap.count(), 1);
    }

    m_obstaclesMap[frontSensorValueInCm]->setDistance(sideSensorValueInCm, m_pixelRatio);

    findNearestObstacleDistance();
    removePeaks(frontSensorValueInCm);
    updateModel();
}

QHash<int, QByteArray> ObstaclesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DistanceFromSensorRole]   = "distanceFromSensor";
    roles[VerticalPositionRole]     = "verticalPosition";
    roles[Color]                    = "obstacleColor";

    return roles;
}

QVariant ObstaclesModel::data(const QModelIndex& index, int role) const
{
    auto obstacle = m_obstaclesMap.values()[index.row()];

    switch (role) {
    case DistanceFromSensorRole:
        return obstacle->distanceFromSensorInPx();
    case VerticalPositionRole:
        return obstacle->verticalPositionInPx();
    case Color:
        return obstacle->color();
    }
    return QVariant();
}

float ObstaclesModel::obstacleWidth() const
{
    return m_obstacleWidth;
}

float ObstaclesModel::obstacleHeight() const
{
    return m_obstacleWidth;
}

bool ObstaclesModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    Q_UNUSED(count);

    beginInsertRows(QModelIndex(), row, row);
    m_obstaclesMap[m_currentFrontSensorValue] = m_currentlyAddedObstacle;
    endInsertRows();

    return true;
}

int ObstaclesModel::rowCount(const QModelIndex& index) const
{
    Q_UNUSED(index);
    return m_obstaclesMap.count();
}
