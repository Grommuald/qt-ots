#ifndef MESSAGE_H
#define MESSAGE_H

#include "sl_protocolcommands.h"
#include "sl_argument.h"

#include <QVector>
#include <QtGlobal>

#include <memory>

namespace SimulationLib
{
struct IncomingMessage
{
    IncomingMessage(int size);
    ~IncomingMessage();

    int size;
    int indexer;
    QByteArray* content;
};

struct ParsedMessage
{
    SensorResponse      type;
    QVector<Argument>   argument;
};

class MessageParser
{
public:
    static SensorResponse                   type (IncomingMessage* message);
    static std::unique_ptr<ParsedMessage>   parse(IncomingMessage* message);
};
}

#endif // MESSAGE_H
