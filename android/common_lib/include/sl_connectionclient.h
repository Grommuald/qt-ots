#ifndef CONNECTIONCLIENT_H
#define CONNECTIONCLIENT_H

#include "sl_message.h"
#include "sl_connectionstatus.h"

#include <QVector>

class QTcpSocket;

namespace SimulationLib
{
class ConnectionClient : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimulationLib::ConnectionStatus::Enum status
        MEMBER m_connectionStatus
        NOTIFY statusChanged
    )
    Q_PROPERTY(QString port     MEMBER m_port)
    Q_PROPERTY(QString address  MEMBER m_hostAddress)

public:
    explicit ConnectionClient(QObject* parent = nullptr);

    Q_INVOKABLE void setConnection();

    void sendResponse       (const ClientResponse& response);
    void setConnectionStatus(const ConnectionStatus::Enum&);

    virtual void onConnectedToServer() = 0;

    static SensorResponse getResponse(IncomingMessage* message);

signals:
    void statusChanged      (const ConnectionStatus::Enum&);
    void sensorMessage      (IncomingMessage* message);

private slots:
    void _onReadyRead();
    void _onConnectedToServer();
    void _onDisconnectedByServer();
    void _onConnectionError();

private:
    void establishConnection();

    QString m_port;
    QString m_hostAddress;
    ConnectionStatus::Enum m_connectionStatus;
    QTcpSocket* m_socket;
    IncomingMessage* m_incomingMessage;
};
}
#endif
