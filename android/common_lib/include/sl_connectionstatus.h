#ifndef CONNECTIONSTATUS_H
#define CONNECTIONSTATUS_H

#include <QObject>

namespace SimulationLib
{
Q_NAMESPACE
class ConnectionStatus : public QObject
{
    Q_OBJECT
public:
    enum class Enum : quint8 {
        Connected,
        Connecting,
        Disconnected,
        Failure
    };
    Q_ENUM(Enum)
};
}
#endif
