#ifndef SL_DEBUG_H
#define SL_DEBUG_H

#include <QDebug>
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define DebugLog (qDebug().noquote() << QString(__FILENAME__).leftJustified(24, ' ') << ' ' << QString(QString::number(__LINE__)).leftJustified(4, ' ') << ' ')

#endif
