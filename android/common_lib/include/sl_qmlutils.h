#ifndef QMLUTILS_H
#define QMLUTILS_H

template <typename QMLType, typename StandardType>
struct QmlUtils
{
    static QMLType qmlWrapper(const StandardType& srcType)
    {
        return static_cast<QMLType>(static_cast<int>(srcType));
    }

    static QMLType qmlWrapper(const int& srcType)
    {
        return static_cast<QMLType>(srcType);
    }

    static StandardType qmlUnwrapper(const QMLType& srcType)
    {
        return static_cast<StandardType>(static_cast<int>(srcType));
    }
};

#endif
