#include "..\include\sl_connectionclient.h"
#include "..\include\sl_protocolcommands.h"
#include "..\include\sl_byteutils.h"
#include "..\include\sl_message.h"
#include "..\include\sl_debug.h"
using namespace SimulationLib;

#include <QtNetwork>
#include <QTcpSocket>

namespace SimulationLib
{
    /*
     * 1. byte - 1-byte ServerCommand
     * 2. byte - 1-byte FrameType
     * 3. byte - 1/2 uint16_t
     * 4. byte - 2/2 uint16_t
     * 5. byte - 1/2 uint16_t
     * 6. byte - 2/2 uint16_t
     */

const uint8_t FrameSize = 6;
const uint8_t ResponseBytePosition = 0;

ConnectionClient::ConnectionClient(QObject *parent)
    : QObject(parent),
      m_socket(new QTcpSocket(this)),
      m_incomingMessage(new IncomingMessage(FrameSize))
{
    connect(m_socket, &QTcpSocket::connected,
            this, &ConnectionClient::_onConnectedToServer);
    connect(m_socket, &QTcpSocket::disconnected,
            this, &ConnectionClient::_onDisconnectedByServer);
    connect(m_socket, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
            this, &ConnectionClient::_onConnectionError);
    connect(m_socket, &QTcpSocket::readyRead,
            this, &ConnectionClient::_onReadyRead);
    setConnectionStatus(ConnectionStatus::Enum::Disconnected);
}

void ConnectionClient::sendResponse(const ClientResponse& response)
{
    DebugLog << "Sending message to the socket...";

    QByteArray messageToSend;

    for (auto i = 0; i < FrameSize - 1; ++i) {
        messageToSend.append(static_cast<char>(0xFF));
    }
    messageToSend.append(static_cast<char>(response));

    m_socket->write(messageToSend);
}

void ConnectionClient::setConnectionStatus(const ConnectionStatus::Enum& status)
{
    m_connectionStatus = status;
    emit statusChanged(status);
}

void ConnectionClient::setConnection()
{
    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        DebugLog << "Host address: " << m_hostAddress;
        DebugLog << "Port: " << m_port.toUShort();

        m_socket->connectToHost(m_hostAddress, m_port.toUShort());
    }
    else {
        _onDisconnectedByServer();
    }
}

void ConnectionClient::_onReadyRead()
{
    auto dataChunk = m_socket->readAll();

    DebugLog << "onReadyRead: dataChunk.count():  " << dataChunk.count();

    for (auto i = 0; i < dataChunk.count(); ++i) {
        m_incomingMessage->content->append(dataChunk.at(i));
        m_incomingMessage->indexer++;

        if (m_incomingMessage->indexer >= m_incomingMessage->size) {
            DebugLog << "m_currentMessage->size: " << m_incomingMessage->size;

            if (m_connectionStatus == ConnectionStatus::Enum::Connected) {
                DebugLog << "emitting server message...";
                emit sensorMessage(m_incomingMessage);
            }
            else {
                auto response = getResponse(m_incomingMessage);
                if (response == SensorResponse::Ok) {
                    setConnectionStatus(ConnectionStatus::Enum::Connected);
                }
                else {
                    setConnectionStatus(ConnectionStatus::Enum::Failure);
                }
            }
            m_incomingMessage = new IncomingMessage(FrameSize);
        }
    }
}

void ConnectionClient::_onConnectedToServer()
{
    setConnectionStatus(ConnectionStatus::Enum::Connected);
    onConnectedToServer();
}

void ConnectionClient::_onDisconnectedByServer()
{
    DebugLog << "disconnectedByServer entered...";
    if (m_connectionStatus != ConnectionStatus::Enum::Failure) {
        setConnectionStatus(ConnectionStatus::Enum::Disconnected);
        m_socket->abort();
    }
}

void ConnectionClient::_onConnectionError()
{
    DebugLog << "connectionError entered...";
    setConnectionStatus(ConnectionStatus::Enum::Failure);
    m_socket->abort();
}

SensorResponse ConnectionClient::getResponse(IncomingMessage* message)
{
    auto parsedMessage = MessageParser::parse(message);
    auto response = static_cast<SensorResponse>(static_cast<quint8>(parsedMessage->argument.at(ResponseBytePosition)));

    return response;
}

}
