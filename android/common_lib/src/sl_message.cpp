#include "..\include\sl_message.h"
#include "..\include\sl_byteutils.h"
#include "..\include\sl_protocolcommands.h"
#include "..\include\sl_debug.h"

namespace SimulationLib
{
IncomingMessage::IncomingMessage(int size)
    : size(size), indexer(0)
{
    content = new QByteArray();
}

IncomingMessage::~IncomingMessage()
{
    if (content != nullptr)
        delete content;
}

SensorResponse MessageParser::type(IncomingMessage *message)
{
    return static_cast<SensorResponse>(message->content->at(0));
}

std::unique_ptr<ParsedMessage> MessageParser::parse(IncomingMessage *message)
{
    auto messageType = type(message);
    std::unique_ptr<ParsedMessage> resultMessage = std::make_unique<ParsedMessage>();

    resultMessage->type = messageType;

    if (messageType == SensorResponse::Frame) {
        /*
         * One-sensor:      SInt16, UInt16
         * Two-sensor:      UInt16, UInt16
         * Four-sensor:     UInt16, UInt16, UInt16, UInt16, ...
         * Eight-sensor:    UInt16, UInt16, UInt16, UInt16, UInt16, UInt16, UInt16, UInt16, ...
         */

        FrameType frameType = static_cast<FrameType>(message->content->at(1));
        // Add frameType at the beginning of the Argument stream
        resultMessage->argument.push_back(ArgumentFactory::createUInt8(static_cast<quint8>(frameType)));

        switch (frameType)
        {
        case FrameType::OneSensor:
        {
            const int StartingByteOffset    = 1;
            const int OneSensorByteStep     = 4;

            for (int i = StartingByteOffset; i < message->size; i += OneSensorByteStep)
            {
                QByteArray byteHolder;
                byteHolder.resize(2);

                byteHolder[0] = message->content->at(i);
                byteHolder[1] = message->content->at(i+1);

                resultMessage->argument.push_back(
                    ArgumentFactory::createSInt16(ByteUtils::toSInt16(byteHolder))
                );

                byteHolder[0] = message->content->at(i+2);
                byteHolder[1] = message->content->at(i+3);

                resultMessage->argument.push_back(
                    ArgumentFactory::createUInt16(ByteUtils::toUInt16(byteHolder))
                );
            }
        }   break;

        case FrameType::TwoSensor:
        {
            const int StartingByteOffset    = 2;
            const int TwoSensorByteStep     = 2;

            for (int i = StartingByteOffset; i < message->size; i += TwoSensorByteStep)
            {
                QByteArray byteHolder;
                byteHolder.resize(2);

                byteHolder[0] = message->content->at(i);
                byteHolder[1] = message->content->at(i+1);

                resultMessage->argument.push_back(
                    ArgumentFactory::createUInt16(ByteUtils::toUInt16(byteHolder))
                );
            }
        }   break;
        }
    }
    else if (messageType == SensorResponse::Ok
          || messageType == SensorResponse::Nok)
    {
        DebugLog << "Sensor response: ";
        QByteArray SensorResponse;

        SensorResponse.append(message->content->at(0));
        resultMessage->argument.push_back(
            ArgumentFactory::createUInt8(ByteUtils::toUInt8(SensorResponse))
        );
    }
    return resultMessage;
}
}
