#include "..\include\sl_argument.h"

namespace SimulationLib
{
int Argument::convert() const
{
    switch (type)
    {
    case ValueType::UInt16: return value.uint16_val;
    case ValueType::SInt16: return value.sint16_val;
    case ValueType::UInt8:  return value.uint8_val;
    case ValueType::SInt8:  return value.sint8_val;
    default:                return 0;
    }
}

QByteArray Argument::fetch() const
{
    QByteArray result;

    switch (type)
    {
    case ValueType::UInt16:
        result.append(static_cast<char>((value.uint16_val >> 8) & 0xFF));
        result.append(static_cast<char>(value.uint16_val & 0xFF));     
        break;
    case ValueType::SInt16:
        result.append(static_cast<char>((value.sint16_val >> 8) & 0xFF));
        result.append(static_cast<char>(value.sint16_val & 0xFF));
        break;
    case ValueType::UInt8:
        result.append(static_cast<char>(value.uint8_val & 0xFF));
        break;
    case ValueType::SInt8:
        result.append(static_cast<char>(value.sint8_val & 0xFF));
        break;
    }
    return result;
}

Argument::operator qint8() const
{
    return value.sint8_val;
}

Argument::operator quint8() const
{
    return value.uint8_val;
}

Argument::operator qint16() const
{
    return value.sint16_val;
}

Argument::operator quint16() const
{
    return value.uint16_val;
}

Argument ArgumentFactory::createUInt16(const quint16& value)
{
    Argument result;

    result.type = ValueType::UInt16;
    result.value.uint16_val = value;

    return result;
}

Argument ArgumentFactory::createSInt16(const qint16& value)
{
    Argument result;

    result.type = ValueType::SInt16;
    result.value.sint16_val = value;

    return result;
}

Argument ArgumentFactory::createUInt8(const quint8& value)
{
    Argument result;

    result.type = ValueType::UInt8;
    result.value.uint8_val = value;

    return result;
}

Argument ArgumentFactory::createSInt8(const qint8& value)
{
    Argument result;

    result.type = ValueType::SInt8;
    result.value.sint8_val = value;

    return result;
}
}
