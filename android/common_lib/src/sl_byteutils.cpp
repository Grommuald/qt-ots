#include "..\include\sl_byteutils.h"
#include "..\include\sl_debug.h"

namespace SimulationLib
{
/*
 * Bytes are read in Big Endian notation.
 */

quint16 ByteUtils::toUInt16(const QByteArray &array)
{
    if (array.count() < 2)
        return 0;

    quint16 result = 0;

    result |= static_cast<quint8>(array[0]);
    result <<= 8;
    result |= static_cast<quint8>(array[1]);

    return result;
}

qint16 ByteUtils::toSInt16(const QByteArray &array)
{
    // might be invalid as there are some problems with
    // int8 -> uint8 conversion

    if (array.count() < 2)
        return 0;

    qint16 result = 0;

    result |= static_cast<quint8>(array[0]);
    result <<= 8;
    result |= static_cast<quint8>(array[1]);

    DebugLog << "result: " << result;

    return result;
}

quint8 ByteUtils::toUInt8(const QByteArray &array)
{
    if (array.count() < 1)
        return 0;

    quint8 result = 0;

    result |= static_cast<quint8>(array[0]);

    return result;
}

qint8 ByteUtils::toSInt8(const QByteArray &array)
{
    if (array.count() < 1)
        return 0;

    qint8 result = 0;

    result |= array[0];

    return result;
}
}
