#include "visualisationclient.h"
using namespace SimulationLib;

VisualisationClient::VisualisationClient(QObject* parent)
    : ConnectionClient(parent)
{

}

void VisualisationClient::onConnectedToServer()
{
    sendResponse(ClientResponse::SayHiAsClient);
}
