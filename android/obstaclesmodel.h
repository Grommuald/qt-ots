#ifndef OBSTACLESMODEL_H
#define OBSTACLESMODEL_H

#include "../common_lib/include/sl_dominantqueue.h"

#include <QObject>
#include <QPoint>
#include <QAbstractListModel>
#include <QList>
#include <QMap>

class Obstacle : public QObject
{
    Q_OBJECT
public:
    explicit Obstacle(QObject* parent = nullptr);
    void setDistance(int newDistanceInCm, float pixelRatio = 1.0f);
    void setVerticalPosition(float newVerticalPositionInPx);

    float verticalPositionInPx()   const;
    float distanceFromSensorInPx() const;
    int distanceFromSensorInCm()   const;
    QString color()                const;

private:
    int     m_distanceFromSensorInCm;
    float   m_distanceFromSensorInPx;
    float   m_verticalPositionInPx;
    QString m_color;

    SimulationLib::DominantQueue m_dominantDistanceQueue;
};

class ObstaclesModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY (QString distanceMarkerColor         READ distanceMarkerColor         NOTIFY nearestObstacleDistanceChanged)
    Q_PROPERTY (int     nearestObstacleDistanceInCm READ nearestObstacleDistanceInCm NOTIFY nearestObstacleDistanceChanged)
    Q_PROPERTY (float   nearestObstacleDistanceInPx READ nearestObstacleDistanceInPx NOTIFY nearestObstacleDistanceChanged)
    Q_PROPERTY (float   obstacleWidth               READ obstacleWidth               NOTIFY obstacleWidthChanged)
    Q_PROPERTY (float   obstacleHeight              READ obstacleHeight              NOTIFY obstacleHeightChanged)

public:
    enum ObstaclesRoles {
        DistanceFromSensorRole = Qt::UserRole + 1,
        VerticalPositionRole,
        Color
    };

    explicit ObstaclesModel(QObject* parent = nullptr);
    ~ObstaclesModel() override;

    void updateObstacleInfo(int frontSensorValue, int sideSensorValue);
    QVariant data(const QModelIndex& index, int role) const override;

    float obstacleWidth()               const;
    float obstacleHeight()              const;
    float nearestObstacleDistanceInPx() const;
    int   nearestObstacleDistanceInCm() const;
    QString distanceMarkerColor()       const;

    void setPixelRatio      (float pixelRatio);
    void setObstacleWidth   (float width);
    void setObstacleHeight  (float height);

signals:
    void nearestObstacleDistanceChanged();
    void obstacleWidthChanged();
    void obstacleHeightChanged();

private:
    float   m_pixelRatio;
    float   m_obstacleWidth;
    float   m_obstacleHeight;
    float   m_nearestObstacleDistanceInPx;
    int     m_nearestObstacleDistanceInCm;
    QString m_distanceMarkerColor;

    Obstacle*             m_currentlyAddedObstacle;
    int                   m_currentFrontSensorValue;
    QMap<int, Obstacle*>  m_obstaclesMap;

    void findNearestObstacleDistance();
    void removePeaks(int);
    void updateModel();

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex&) const override;
    bool insertRows(int row,
                    int count,
                    const QModelIndex& parent = QModelIndex()) override;
};
#endif
