import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4

Button {
    property bool isPullable: true

    id: button
    x: -width * 0.8
    width: 80
    height: 50
    state: "hidden"

    states: [
        State {
            name: "hidden"
            when: !hovered && isPullable
            PropertyChanges {
                target: button
                x: -width * 0.8
            }
        },
        State {
            name: "extended"
            when: hovered && isPullable
            PropertyChanges {
                target: button
                x: 0
            }
        }
    ]

    transitions: [
        Transition {
            to: "hidden"
            NumberAnimation { properties: "x"; easing.type: Easing.InOutQuad; duration: 500 }
        },
        Transition {
            to: "extended"
            NumberAnimation { properties: "x"; easing.type: Easing.InOutQuad; duration: 500 }
        }

    ]
}

