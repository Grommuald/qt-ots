QT += quick core quickcontrols2 widgets network
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    obstaclesmodel.cpp \
    protocolstatemachine.cpp \
    simulationcontroller.cpp \
    visualisationclient.cpp \
    common_lib/src/sl_connectionclient.cpp \
    common_lib/src/sl_argument.cpp \
    common_lib/src/sl_byteutils.cpp \
    common_lib/src/sl_dominantqueue.cpp \
    common_lib/src/sl_message.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    obstaclesmodel.h \
    protocolstatemachine.h \
    simulationcontroller.h \
    visualisationclient.h \
    common_lib/include/sl_argument.h \
    common_lib/include/sl_byteutils.h \
    common_lib/include/sl_connectionclient.h \
    common_lib/include/sl_connectionstatus.h \
    common_lib/include/sl_debug.h \
    common_lib/include/sl_dominantqueue.h \
    common_lib/include/sl_message.h \
    common_lib/include/sl_protocolcommands.h \
    common_lib/include/sl_qmlutils.h \
    common_lib/include/sl_types.h

DISTFILES += \
    android-sources/AndroidManifest.xml

android:ANDROID_PACKAGE_SOURCE_DIR=$$PWD/android-sources
